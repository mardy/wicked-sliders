#! /usr/bin/env python3

from PIL import Image
from pathlib import Path
import sys
import argparse

parser = argparse.ArgumentParser(prog='img2c')
parser.add_argument('-c', '--color-key')
parser.add_argument('-f', '--format')
parser.add_argument('image', help='image to embed')
args = parser.parse_args()

with Image.open(args.image) as img:
    file_path = Path(img.filename)
    object_name = file_path.stem
    width = img.width
    height = img.height
    has_alpha = False
    if args.format:
        new_mode = args.format
        # Hackish, but for now it's good
        bpp = len(new_mode)
    else:
        if img.mode == 'RGBA':
            has_alpha = True
        if img.info.get("transparency", None) is not None:
            has_alpha = True
        bpp = 4 if has_alpha else 3
        if bpp == 3 and args.color_key:
            bpp = 4
        new_mode = 'RGB' if bpp == 3 else 'RGBA'

    img = img.convert(new_mode)
    if args.color_key:
        pixdata = img.load()
        color_key = int(args.color_key, 0)
        red = (color_key & 0xff0000) >> 16
        green = (color_key & 0x00ff00) >> 8
        blue = (color_key & 0xff)

        for y in range(height):
            for x in range(width):
                if pixdata[x, y] == (red, green, blue, 255):
                    pixdata[x, y] = (0, 0, 0, 0)

    contents = img.tobytes()
    with open(object_name + '.h', 'w') as out:
        out.write(f'''
#include <stdint.h>

static const struct {{
    int width;
    int height;
    char bytes_per_pixel;
    uint8_t pixels[{width} * {height} * {bpp} + 1];
}} img_{object_name} = {{
    {width}, {height}, {bpp},
''')
        size = len(contents)
        for i in range(0, size, 16):
            row = contents[i:i+16]
            out.write('    "')
            hex_row = ''.join(f'\\{b:03o}' for b in row)
            out.write(hex_row + '"\n')
        out.write('};\n')
