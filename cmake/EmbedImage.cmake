function(embed_image)
    set(oneValueArgs COLORKEY DESTINATION FORMAT)
    set(multiValueArgs IMAGES)
    cmake_parse_arguments(EMBED_IMAGE "" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
    set(result)
    set(extra_params)
    set(out_var ${EMBED_IMAGE_DESTINATION})
    if(DEFINED EMBED_IMAGE_COLORKEY)
        list(APPEND extra_params "-c" "${EMBED_IMAGE_COLORKEY}")
    endif()
    if(DEFINED EMBED_IMAGE_FORMAT)
        list(APPEND extra_params "-f" "${EMBED_IMAGE_FORMAT}")
    endif()
    foreach(in_f ${EMBED_IMAGE_IMAGES})
        get_filename_component(basename ${in_f} NAME_WE)
        set(out_f "${CMAKE_CURRENT_BINARY_DIR}/${basename}.h")

        add_custom_command(
            OUTPUT ${out_f}
            COMMAND ${CMAKE_SOURCE_DIR}/tools/img2c.py ${extra_params} ${CMAKE_CURRENT_SOURCE_DIR}/${in_f}
            DEPENDS ${in_f} ${CMAKE_SOURCE_DIR}/tools/img2c.py
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
            COMMENT "Embedding ${in_f} as a resource"
        )
        list(APPEND result ${out_f})
    endforeach()
    set(${out_var} ${result} PARENT_SCOPE)
endfunction()
