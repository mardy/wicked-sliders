#include "joystick_input.h"

#include <climits>

JoystickInput::JoystickInput(SDL_Joystick *joy):
    m_buttonMapping{0, 1},
    m_swapAxes(false),
    m_joy(joy)
{
    m_buttons = SDL_JoystickNumButtons(joy);
    m_axes = SDL_JoystickNumAxes(joy);
    m_hats = SDL_JoystickNumHats(joy);
    const std::string model = name();
    if (model.rfind("Wiimote", 0) == 0) {
        // 0 and 1 are A and B, while we want buttons "1" and "2"
        m_buttonMapping[0] = 2;
        m_buttonMapping[1] = 3;
        m_swapAxes = true;
        m_axes = 0;
    } else if (model.find("Wacom") != std::string::npos ||
               model.find("LED Controller") != std::string::npos) {
        m_isUsable = false;
    }
}

bool JoystickInput::update()
{
    m_x = m_y = 0;
    if (m_axes >= 2) {
        m_x += SDL_JoystickGetAxis(m_joy, 0);
        m_y += SDL_JoystickGetAxis(m_joy, 1);
    }
    switch (SDL_JoystickGetHat(m_joy, 0)) {
    case SDL_HAT_LEFT: m_x -= SHRT_MAX; break;
    case SDL_HAT_RIGHT: m_x += SHRT_MAX; break;
    case SDL_HAT_UP: m_y -= SHRT_MAX; break;
    case SDL_HAT_DOWN: m_y += SHRT_MAX; break;
    case SDL_HAT_RIGHTUP: m_x += SHRT_MAX; m_y -= SHRT_MAX; break;
    case SDL_HAT_RIGHTDOWN: m_x += SHRT_MAX; m_y += SHRT_MAX; break;
    case SDL_HAT_LEFTUP: m_x -= SHRT_MAX; m_y -= SHRT_MAX; break;
    case SDL_HAT_LEFTDOWN: m_x -= SHRT_MAX; m_y += SHRT_MAX; break;
    }
    if (m_swapAxes) {
        int tmp = m_x;
        m_x = m_y;
        m_y = tmp;
    }
    return true;
}

bool JoystickInput::buttonIsPressed(int index)
{
    return SDL_JoystickGetButton(m_joy, m_buttonMapping[index]);
}

std::string JoystickInput::name() const
{
    return SDL_JoystickName(m_joy);
}
