#ifndef WS_CAR_PARAMETERS_H
#define WS_CAR_PARAMETERS_H

/* Constant parameters of a vehicle model */
struct CarParameters {
    float speed_max;
    float speed_min;
    float acceleration_max;
    float linearDamping;
    float angularDamping;
};

#endif // WS_CAR_PARAMETERS_H
