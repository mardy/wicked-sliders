#ifndef WS_RACE_END_H
#define WS_RACE_END_H

#include <SDL.h>
#include <memory>

class FontRenderer;
class Player;
struct GameData;

class RaceEndPrivate;
class RaceEnd {
public:
    RaceEnd(FontRenderer *playerFont, const GameData &gameData,
            Player *leadPlayer);
    virtual ~RaceEnd();

    bool isDone() const;

    void update(int elapsedMs);

    void blit(SDL_Renderer *renderer, int off_x, int off_y);

private:
    std::unique_ptr<RaceEndPrivate> impl;
};

#endif // WS_RACE_END_H
