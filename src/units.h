#ifndef WS_UNITS_H
#define WS_UNITS_H

inline float p2m(int pixels) {
    return pixels / 8.0;
}

inline int m2p(float meters) {
    return int(meters * 8);
}

#endif // WS_UNITS_H
