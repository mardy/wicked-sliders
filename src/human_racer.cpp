#include "human_racer.h"

#include "input_device.h"

class HumanRacerPrivate {
public:
    HumanRacerPrivate(InputDevice *inputDevice);

private:
    friend class HumanRacer;
    InputDevice *m_input;
};

HumanRacerPrivate::HumanRacerPrivate(InputDevice *inputDevice):
    m_input(inputDevice)
{
}

HumanRacer::HumanRacer(const std::string &name,
                       InputDevice *inputDevice,
                       const CarModel &carModel,
                       RgbColor color):
    Racer(name, carModel, color),
    impl(new HumanRacerPrivate(inputDevice))
{
}

HumanRacer::~HumanRacer() = default;

void HumanRacer::updateInput(const GameData &)
{
    InputDevice *input = impl->m_input;
    input->update();

    float steer = input->xAxis();
    float acceleration;
    if ((input->axesCount() > 0 || input->hatsCount() > 0) &&
        input->buttonCount() >= 2) {
        // Give precedence to break
        acceleration = input->buttonIsPressed(Buttons::Back) ? -1.0 :
            (input->buttonIsPressed(Buttons::OK) ? 1.0 : 0.0);

    } else {
        acceleration = -input->yAxis();
    }
    setControls(steer, acceleration);
}
