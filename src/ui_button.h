#ifndef WS_UI_BUTTON_H
#define WS_UI_BUTTON_H

#include <SDL.h>
#include <memory>
#include <string_view>

class UIButtonPrivate;
class UIButton {
public:
    UIButton(const std::string_view &text, int w, int h);
    ~UIButton();

    void setFocused(bool focused);

    void blit(SDL_Renderer *renderer, int x, int y);

private:
    std::unique_ptr<UIButtonPrivate> impl;
};

#endif // WS_UI_BUTTON_H
