#include "fonts.h"

#define DECLARE_EMBEDDED_FILE(basename) \
    extern char _binary_ ## basename ## _start[], _binary_ ## basename ## _end[]

#define USE_EMBEDDED_FILE(basename) \
    _binary_ ## basename ## _start, \
    size_t(_binary_ ## basename ## _end - _binary_ ## basename ## _start)

DECLARE_EMBEDDED_FILE(assets_fonts_nevduplenysh_otf);
DECLARE_EMBEDDED_FILE(assets_fonts_boorsok_otf);

static const FontFile embeddedFonts[] = {
    { USE_EMBEDDED_FILE(assets_fonts_nevduplenysh_otf) },
    { USE_EMBEDDED_FILE(assets_fonts_boorsok_otf) },
};

const FontFile &Fonts::fontFile(Fonts::Type type)
{
    switch (type) {
    case Fonts::Type::Title:
        return embeddedFonts[1];
    default:
        return embeddedFonts[0];
    }
}
