#include "sprite_car.h"

#include "embedded_image.h"

#include "car_0.h"
#include "car_0_fixed.h"

#include <algorithm>

SpriteCar::SpriteCar(SDL_Renderer *renderer,
                     const EmbeddedImage *imgColor,
                     const EmbeddedImage *imgGrey,
                     RgbColor color):
    m_texture(nullptr)
{
    int w = imgGrey->width;
    int h = imgGrey->height;
    int bytes_per_pixel = imgGrey->bytes_per_pixel;
    uint8_t *pixels = new uint8_t[w * h * bytes_per_pixel];
    int num_pixels = w * h;
    for (int i = 0; i < num_pixels; i++) {
        uint8_t *pixel = &pixels[i * 4];
        uint8_t luminosity = imgGrey->pixels[i * 4];
        uint8_t alpha = imgGrey->pixels[i * 4 + 3];
        if (alpha > 0) {
            pixel[0] = std::min(color.r * luminosity / 128, 255);
            pixel[1] = std::min(color.g * luminosity / 128, 255);
            pixel[2] = std::min(color.b * luminosity / 128, 255);
            pixel[3] = alpha;
        } else {
            pixel[0] = imgColor->pixels[i * 4];
            pixel[1] = imgColor->pixels[i * 4 + 1];
            pixel[2] = imgColor->pixels[i * 4 + 2];
            pixel[3] = imgColor->pixels[i * 4 + 3];
        }
    }

    SDL_Surface *surface =
        SDL_CreateRGBSurfaceWithFormatFrom(pixels,
                                 w, h,
                                 bytes_per_pixel * 8,
                                 bytes_per_pixel * w,
                                 SDL_PIXELFORMAT_RGBA32);
    m_texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
}

SpriteCar::~SpriteCar()
{
    SDL_DestroyTexture(m_texture);
}

void SpriteCar::blit(SDL_Renderer *dest, int dest_x, int dest_y, float angle)
{
    SDL_Rect destRect;
    SDL_QueryTexture(m_texture, nullptr, nullptr, &destRect.w, &destRect.h);
    destRect.x = dest_x - destRect.w / 2;
    destRect.y = dest_y - destRect.h / 2;
    SDL_RenderCopyEx(dest, m_texture, nullptr, &destRect,
                     -angle * 180 / M_PI, nullptr, SDL_FLIP_NONE);
}
