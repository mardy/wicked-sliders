#ifndef WS_RACER_H
#define WS_RACER_H

#include "car_data.h"
#include "color.h"

#include <SDL.h>
#include <memory>
#include <string>

class GameData;
struct CarModel;
class b2Body;
class b2World;

class RacerPrivate;
class Racer {
public:
    virtual ~Racer();

    const std::string &name() const;
    RgbColor color() const;

    /* Get input from the player or AI; this should lead to a call to
     * setControls() */
    virtual void updateInput(const GameData &gameData) = 0;

    void populateWorld(b2World &world, int index);
    b2Body *body() const;
    int playerIndex() const;
    virtual void blit(SDL_Renderer *dest, int off_x, int off_y);

    void setLevel(CarLevel level);

    CarData &carData();
    const CarData &carData() const;

protected:
    Racer(const std::string &name, const CarModel &carModel,
          RgbColor color);

    /* steer: negative for left, positive for right.
     * acceleration: negative for breaking, positive for gas.
     */
    void setControls(float steer, float acceleration);

    void setBaseFilterMask(uint16_t filterMask);

private:
    std::unique_ptr<RacerPrivate> impl;
};

#endif // WS_RACER_H
