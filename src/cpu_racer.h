#ifndef WS_CPU_RACER_H
#define WS_CPU_RACER_H

#include "racer.h"

struct CarModel;

class CpuRacerPrivate;
class CpuRacer: public Racer {
public:
    enum Level {
        Novice = 0,
        Intermediate,
        Expert,
        NumLevels,
    };

    CpuRacer(const std::string &name,
             Level level,
             const CarModel &carModel,
             RgbColor color);
    virtual ~CpuRacer();

    void updateInput(const GameData &gameData) override;
    void blit(SDL_Renderer *dest, int off_x, int off_y) override;

private:
    std::unique_ptr<CpuRacerPrivate> impl;
};

#endif // WS_CPU_RACER_H
