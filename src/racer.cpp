#include "racer.h"

#include "box2d_glue.h"
#include "car_model.h"
#include "sprite_car.h"
#include "units.h"

#include <b2_body.h>
#include <b2_polygon_shape.h>
#include <b2_world.h>
#include <memory>

class RacerPrivate {
public:
    RacerPrivate(const std::string &name, const CarModel &carModel,
                 RgbColor color);

private:
    friend class Racer;
    std::string m_name;
    RgbColor m_color;
    const CarModel &m_carModel;
    uint16_t m_baseFilterMask;
    std::unique_ptr<SpriteCar> m_car;
    CarData m_carData;
    b2Body *m_body;
};

RacerPrivate::RacerPrivate(const std::string &name,
                           const CarModel &carModel,
                           RgbColor color):
    m_name(name),
    m_color(color),
    m_carModel(carModel),
    m_baseFilterMask(0),
    m_carData(),
    m_body(nullptr)
{
    m_carData.params = carModel.params;
}

Racer::Racer(const std::string &name,
             const CarModel &carModel,
             RgbColor color):
    impl(new RacerPrivate(name, carModel, color))
{
}

Racer::~Racer() = default;

const std::string &Racer::name() const
{
    return impl->m_name;
}

RgbColor Racer::color() const
{
    return impl->m_color;
}

void Racer::populateWorld(b2World &world, int index)
{
    UserData userData(BodyType::Racer, index);
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.linearDamping = impl->m_carData.params.linearDamping;
    bodyDef.angularDamping = impl->m_carData.params.angularDamping;
    bodyDef.awake = true;
    bodyDef.position.Set(p2m(impl->m_carData.x),
                         p2m(impl->m_carData.y));
    bodyDef.angle = impl->m_carData.angle;
    bodyDef.userData.pointer = userData.pointer;
    impl->m_body = world.CreateBody(&bodyDef);

    b2PolygonShape polygonShape;
    const EmbeddedImage &carImage = *impl->m_carModel.imgColor;
    polygonShape.SetAsBox(p2m(carImage.width / 2), p2m(carImage.height / 2));
    // TODO get density from car model
    impl->m_body->CreateFixture(&polygonShape, 2.0);

    setLevel(CarLevel::Floor);
}

b2Body *Racer::body() const
{
    return impl->m_body;
}

int Racer::playerIndex() const
{
    return UserData(body()->GetUserData()).data.index;
}

void Racer::blit(SDL_Renderer *dest, int off_x, int off_y)
{
    int x = off_x + impl->m_carData.x;
    int y = off_y + impl->m_carData.y;
    float angle = impl->m_carData.angle;

    if (!impl->m_car) {
        impl->m_car.reset(new SpriteCar(dest,
                                        impl->m_carModel.imgColor,
                                        impl->m_carModel.imgGrey,
                                        impl->m_color));
    }
    impl->m_car->blit(dest, x, y, angle);
}

void Racer::setLevel(CarLevel level)
{
    fprintf(stderr, "Car level now %d\n", uint16_t(level));
    impl->m_carData.level = level;

    b2Fixture *carFixture = body()->GetFixtureList();
    b2Filter filter;
    uint16_t category = 0;
    uint16_t filterMask = impl->m_baseFilterMask;
    switch (level) {
    case CarLevel::RampUpwards:
        category |= uint16_t(CollisionCategory::Ramp |
                             CollisionCategory::Floor);
        filterMask |= uint16_t(CollisionCategory::Ramp |
                               CollisionCategory::Floor);
        break;
    case CarLevel::RampDownwards:
        category |= uint16_t(CollisionCategory::Ramp |
                             CollisionCategory::Upper);
        filterMask |= uint16_t(CollisionCategory::Ramp |
                               CollisionCategory::Upper);
        break;
    case CarLevel::Upper:
        category |= uint16_t(CollisionCategory::Upper);
        filterMask |= uint16_t(CollisionCategory::Upper |
                               CollisionCategory::Ramp);
        break;
    case CarLevel::Floor:
    default:
        category |= uint16_t(CollisionCategory::Floor);
        filterMask |= uint16_t(CollisionCategory::Floor |
                               CollisionCategory::Ramp |
                               CollisionCategory::UnderBridgeWall);
        break;
    }
    filter.categoryBits = category;
    filter.maskBits = filterMask;
    carFixture->SetFilterData(filter);
}

CarData &Racer::carData()
{
    return impl->m_carData;
}

const CarData &Racer::carData() const
{
    return impl->m_carData;
}

void Racer::setControls(float steer, float acceleration)
{
    impl->m_carData.steer = steer;
    impl->m_carData.acceleration = acceleration;
}

void Racer::setBaseFilterMask(uint16_t filterMask)
{
    impl->m_baseFilterMask = filterMask;
}
