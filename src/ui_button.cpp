#include "ui_button.h"

#include "font_renderer.h"
#include "timer.h"

#include <cmath>
#include <string>

static const RgbColor normalColor { 64, 64, 255 };
static const RgbColor focusedColor { 255, 64, 64 };
static const int normalPointSize = 14;
static const int focusedPointSize = 15;

class UIButtonPrivate {
public:
    UIButtonPrivate(const std::string_view &text, int w, int h);
    ~UIButtonPrivate();

    static SDL_Texture *create_texture(SDL_Renderer *renderer,
                                       const std::string &text, int w, int h,
                                       bool isFocused);

    void setFocused(bool focused);
    void blit(SDL_Renderer *dest, int x, int y);

private:
    friend class UIButton;
    std::string m_text;
    int m_w;
    int m_h;
    SDL_Texture *m_texture;
    bool m_isFocused;
};

UIButtonPrivate::UIButtonPrivate(const std::string_view &text, int w, int h):
    m_text(text),
    m_w(w),
    m_h(h),
    m_texture(nullptr),
    m_isFocused(false)
{
}

UIButtonPrivate::~UIButtonPrivate()
{
    if (m_texture) {
        SDL_DestroyTexture(m_texture);
    }
}

SDL_Texture *UIButtonPrivate::create_texture(SDL_Renderer *renderer,
                                             const std::string &text,
                                             int w, int h,
                                             bool isFocused)
{
    RgbColor background = isFocused ? focusedColor : normalColor;
    int pointSize = isFocused ? focusedPointSize : normalPointSize;

    FontRenderer fontRenderer(Fonts::Type::Title, pointSize);
    fontRenderer.setColor({255, 255, 255});

    SDL_Texture *tex = SDL_CreateTexture(renderer,
                                         SDL_PIXELFORMAT_RGB565,
                                         SDL_TEXTUREACCESS_TARGET,
                                         w, h);
    SDL_SetRenderTarget(renderer, tex);
    SDL_SetRenderDrawColor(renderer,
                           background.r, background.g, background.b, 0);
    SDL_RenderClear(renderer);
    fontRenderer.blitText(renderer, w / 2, h / 2, text.c_str());
    SDL_SetRenderTarget(renderer, nullptr);
    return tex;
}

void UIButtonPrivate::setFocused(bool focused)
{
    if (m_isFocused == focused) return;

    if (m_texture) {
        SDL_DestroyTexture(m_texture);
        m_texture = nullptr;
    }

    m_isFocused = focused;
}

void UIButtonPrivate::blit(SDL_Renderer *renderer, int x, int y)
{
    if (!m_texture) {
        m_texture = create_texture(renderer, m_text, m_w - 4, m_h - 4,
                                   m_isFocused);
    }

    if (m_isFocused) {
        SDL_Rect focusRect { x, y, m_w, m_h };
        uint8_t c = 128 + 128 * sinf(Timer::instance()->timeSecs() * 10);
        SDL_SetRenderDrawColor(renderer, c, c, c, 255);
        SDL_RenderFillRect(renderer, &focusRect);
    } else {
        SDL_Rect borderRect {
            x + 1, y + 1,
            m_w - 2, m_h - 2,
        };
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderFillRect(renderer, &borderRect);
    }

    SDL_Rect destRect { x + 2, y + 2 };
    SDL_QueryTexture(m_texture, nullptr, nullptr, &destRect.w, &destRect.h);
    SDL_RenderCopy(renderer, m_texture, nullptr, &destRect);
}

UIButton::UIButton(const std::string_view &text, int w, int h):
    impl(new UIButtonPrivate(text, w, h))
{
}

UIButton::~UIButton() = default;

void UIButton::setFocused(bool focused)
{
    impl->setFocused(focused);
}

void UIButton::blit(SDL_Renderer *dest, int x, int y)
{
    impl->blit(dest, x, y);
}
