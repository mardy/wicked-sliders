#ifndef WS_TIMER_H
#define WS_TIMER_H

#include <SDL.h>
#include <cstdint>

class Timer {
public:
    static Timer *instance() {
        if (!s_instance) {
            s_instance = new Timer();
        }
        return s_instance;
    }

    void frameStarted(uint32_t ticks) {
        m_elapsedSinceLastFrame = ticks - m_ticks;
        m_ticks = ticks;
    }

    int elapsedMs() const { return m_elapsedSinceLastFrame; }
    uint32_t timeMs() const { return m_ticks; }

    float elapsedSecs() const { return elapsedMs() / 1000.0; }
    float timeSecs() const { return timeMs() / 1000.0; }

protected:
    Timer():
        m_ticks(SDL_GetTicks()),
        m_elapsedSinceLastFrame(0),
        m_initialTicks(m_ticks)
    {
    }

private:
    static Timer *s_instance;
    uint32_t m_ticks;
    uint32_t m_elapsedSinceLastFrame;
    uint32_t m_initialTicks;
};

#endif // WS_TIMER_H
