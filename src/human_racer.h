#ifndef WS_HUMAN_RACER_H
#define WS_HUMAN_RACER_H

#include "racer.h"

class InputDevice;
struct CarModel;

class HumanRacerPrivate;
class HumanRacer: public Racer {
public:
    HumanRacer(const std::string &name,
               InputDevice *inputDevice,
               const CarModel &carModel,
               RgbColor color);
    virtual ~HumanRacer();

    void updateInput(const GameData &gameData) override;

private:
    std::unique_ptr<HumanRacerPrivate> impl;
};

#endif // WS_HUMAN_RACER_H
