#ifndef WS_RACE_H
#define WS_RACE_H

#include <SDL.h>
#include <memory>

class Player;
class Timer;
class Track;
struct GameData;

class RacePrivate;
class Race {
public:
    Race();
    virtual ~Race();

    void setTrack(Track *track);

    void setLapCount(int count);

    GameData &gameData();
    const GameData &gameData() const;

    void addPlayer(Player *player);

    void update(Timer *timer);
    bool isDone() const;

    void blit(SDL_Renderer *renderer, int off_x, int off_y);

private:
    std::unique_ptr<RacePrivate> impl;
};

#endif // WS_RACE_H
