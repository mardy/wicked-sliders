#ifndef WS_FONT_RENDERER_H
#define WS_FONT_RENDERER_H

#include "color.h"
#include "fonts.h"

#include <SDL.h>
#include <SDL_ttf.h>
#include <utility>

class FontRenderer {
public:
    FontRenderer(Fonts::Type fontType, int pointSize);
    ~FontRenderer();

    void setColor(RgbColor color);

    void blitText(SDL_Renderer *dest, int x, int y, const char *text);
    void blit(SDL_Renderer *dest, int x, int y, const char *format, ...);

    SDL_Surface *renderText(const char *text) const;
    SDL_Surface *render(const char *format, ...) const;

    std::pair<int16_t,int16_t> size(const char *text) const;

private:
    TTF_Font *m_font;
    RgbColor m_color;
};

#endif // WS_FONT_RENDERER_H
