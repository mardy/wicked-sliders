#ifndef WS_CAR_MODEL_H
#define WS_CAR_MODEL_H

#include "car_parameters.h"
#include "embedded_image.h"

struct CarModel {
    const char *name;

    const EmbeddedImage *imgColor; // Colored image, to be used as-is
    const EmbeddedImage *imgGrey;  // Greyscale image, to be blended

    // Model physical parameters
    CarParameters params;
};

#endif // WS_CAR_MODEL_H
