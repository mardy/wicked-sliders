#ifndef WS_GEOMETRY_H
#define WS_GEOMETRY_H

#include <b2_math.h>

struct Point {
    int16_t x;
    int16_t y;
};

enum class Orientation {
    ClockWise = -1,
    Aligned = 0,
    CounterClockWise = 1,
};

inline Orientation orientation(const b2Vec2 &p0,
                               const b2Vec2 &p1,
                               const b2Vec2 &p2)
{
    int val = (p1.y - p0.y) * (p2.x - p1.x) -
        (p1.x - p0.x) * (p2.y - p1.y);

    if (val == 0) return Orientation::Aligned;
    return (val > 0)? Orientation::ClockWise : Orientation::CounterClockWise;
}

static inline float normalizeAngle(float angle) {
    float a = fmodf(angle, M_PI * 2);
    if (a > M_PI) a -= M_PI * 2;
    else if (a < -M_PI) a += M_PI * 2;
    return a;
}

#endif // WS_GEOMETRY_H
