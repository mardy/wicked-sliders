#ifndef WS_CAR_MODEL_FACTORY_H
#define WS_CAR_MODEL_FACTORY_H

#include "car_model.h"

namespace CarModelFactory {
    extern const int numModels;
    extern const CarModel models[];
};

#endif // WS_CAR_MODEL_FACTORY_H
