#ifndef WS_JOYSTICK_INPUT_H
#define WS_JOYSTICK_INPUT_H

#include "input_device.h"

#include <SDL.h>

class JoystickInput: public InputDevice {
public:
    JoystickInput(SDL_Joystick *joy);

    bool update() override;

    bool buttonIsPressed(int index) override;

    std::string name() const override;

private:
    int m_buttonMapping[2]; // We only use two buttons in this game
    bool m_swapAxes;
    SDL_Joystick *m_joy;
};

#endif // WS_JOYSTICK_INPUT_H
