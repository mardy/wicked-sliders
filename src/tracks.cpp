#include "tracks.h"

#define DECLARE_EMBEDDED_FILE(basename) \
    extern char _binary_ ## basename ## _start[], _binary_ ## basename ## _end[]

#define USE_EMBEDDED_FILE(basename) \
    _binary_ ## basename ## _start, \
    size_t(_binary_ ## basename ## _end - _binary_ ## basename ## _start)

DECLARE_EMBEDDED_FILE(assets_ss_tracks_0AROUND_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_0GUMMI_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_0HAIRPIN_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_0LAINAUS_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_0OILY2_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_0OVAL_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_0SPEEDY_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_0SPOON_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_0WORKS2_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_0YARD2_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_18RATA_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1AITO_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1AREENA_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1ASEMA_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1HELPPO_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1hyv___SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1illit__n_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1KIEMURA_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1KISKOT_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1KOMATON_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1KUMMA_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1LOIVA_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1MAHTAVA_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1OUTO_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1OVELA_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1PERUS_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1PUJOTUS_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1RENTO_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1SILTALA_SS);
DECLARE_EMBEDDED_FILE(assets_ss_tracks_1XRATA_SS);

namespace Tracks {
const TrackFile embeddedTracks[] = {
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0AROUND_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0GUMMI_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0HAIRPIN_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0LAINAUS_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0OILY2_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0OVAL_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0SPEEDY_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0SPOON_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0WORKS2_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_0YARD2_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_18RATA_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1AITO_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1AREENA_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1ASEMA_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1HELPPO_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1hyv___SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1illit__n_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1KIEMURA_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1KISKOT_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1KOMATON_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1KUMMA_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1LOIVA_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1MAHTAVA_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1OUTO_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1OVELA_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1PERUS_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1PUJOTUS_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1RENTO_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1SILTALA_SS) },
    { nullptr, USE_EMBEDDED_FILE(assets_ss_tracks_1XRATA_SS) },
};

int embeddedTracksCount = sizeof(embeddedTracks) / sizeof(embeddedTracks[0]);

} // namespace
