#include "font_renderer.h"

FontRenderer::FontRenderer(Fonts::Type fontType, int pointSize)
{
    const FontFile &fontData = fontFile(fontType);

    SDL_RWops *pFontMem = SDL_RWFromConstMem(fontData.data, fontData.size);
    if (!pFontMem) {
        fprintf(stderr, "Could not read font\n");
        return;
    }

    // Load the font from the memory buffer
    m_font = TTF_OpenFontRW(pFontMem, 1, pointSize);
}

FontRenderer::~FontRenderer()
{
    if (m_font) {
        TTF_CloseFont(m_font);
    }
}

void FontRenderer::setColor(RgbColor color)
{
	m_color = color;
}

void FontRenderer::blitText(SDL_Renderer *dest, int x, int y, const char *text)
{
    SDL_Surface *s = renderText(text);
    SDL_Rect destRect {
        x - s->w / 2, y - s->h / 2,
        s->w, s->h
    };
    SDL_Texture *tex = SDL_CreateTextureFromSurface(dest, s);
    SDL_FreeSurface(s);
    SDL_RenderCopy(dest, tex, nullptr, &destRect);
    SDL_DestroyTexture(tex);
}

void FontRenderer::blit(SDL_Renderer *dest, int x, int y, const char *format, ...)
{
    char buffer[512];
    va_list args;

    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer) - 1, format, args);
    va_end(args);
    buffer[sizeof(buffer) - 1] = 0;

    blitText(dest, x, y, buffer);
}

SDL_Surface *FontRenderer::renderText(const char *text) const
{
    SDL_Color color {m_color.r, m_color.g, m_color.b};
    return TTF_RenderUTF8_Blended(m_font, text, color);
}

SDL_Surface *FontRenderer::render(const char *format, ...) const
{
    char buffer[512];
    va_list args;

    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer) - 1, format, args);
    va_end(args);
    buffer[sizeof(buffer) - 1] = 0;

    return renderText(buffer);
}

std::pair<int16_t,int16_t> FontRenderer::size(const char *text) const
{
    int w, h;
    TTF_SizeUTF8(m_font, text, &w, &h);
    return { int16_t(w), int16_t(h) };
}
