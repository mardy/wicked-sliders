#ifndef WS_PLAYER_H
#define WS_PLAYER_H

#include "color.h"

#include <memory>

class CarModel;
class InputDevice;
class Racer;

class PlayerPrivate;
class Player {
public:
    Player(InputDevice *input);
    Player(Player &&o);
    virtual ~Player();

    void setName(std::string name);
    const std::string &name() const;

    void setColor(RgbColor color);
    RgbColor color() const;

    void setCarModel(const CarModel &carModel);
    const CarModel &carModel() const;

    InputDevice *inputDevice() const;

    Racer *makeRacer() const;

private:
    std::unique_ptr<PlayerPrivate> impl;
};

#endif // WS_PLAYER_H
