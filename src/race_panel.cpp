#include "race_panel.h"

#include "game_data.h"
#include "racer.h"

RacePanel::RacePanel():
    m_playerBoxHeight(32), // TODO: compute based on screen size
    m_playerBoxWidth(120),
    m_playerBoxesPerColumn(3),
    m_playerFont(Fonts::Type::Title, 16)
{
}

RacePanel::~RacePanel() = default;

void RacePanel::update(const GameData &gameData, int elapsedMs)
{
    if (m_playerBoxesAnimator.currentPositions().empty()) {
        PointAnimator::Points points;
        int numPlayers = gameData.racers.size();
        for (int index = 0; index < numPlayers; index++) {
            int row = index % m_playerBoxesPerColumn;
            int column = index / m_playerBoxesPerColumn;
            points.emplace_back(PointAnimator::Point {
                int16_t(column * m_playerBoxWidth),
                int16_t(row * m_playerBoxHeight),
            });
        }
        m_playerBoxesAnimator.setPoints(points);
    }

    int numPlayers = gameData.racers.size();
    for (int i = 0; i < numPlayers; i++) {
        m_playerBoxesAnimator.reorder(i, gameData.racers[i].ranking);
    }

    m_playerBoxesAnimator.setElapsed(elapsedMs);
}

void RacePanel::blit(SDL_Renderer *dest, int off_x, int off_y,
                     const GameData &gameData)
{
    int index = 0;
    const PointAnimator::Points &currentPositions =
        m_playerBoxesAnimator.currentPositions();
    for (const GameData::RacerData &racerData: gameData.racers) {
        const Racer *racer = racerData.racer;
        RgbColor c = racer->color();

        const PointAnimator::Point &pos = currentPositions[index];
        SDL_Rect rect {
            off_x + 1 + pos.x,
            off_y + 1 + pos.y,
            m_playerBoxWidth - 2, m_playerBoxHeight - 2
        };
        SDL_SetRenderDrawColor(dest, c.r, c.g, c.b, 255);
        SDL_RenderFillRect(dest, &rect);
        m_playerFont.setColor({255, 255, 255});
        m_playerFont.blit(dest, rect.x + rect.w / 2, rect.y + rect.h / 2,
                          "%.9s: %d", racer->name().c_str(), racerData.lap);
        index++;
    }
}
