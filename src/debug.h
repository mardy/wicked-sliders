#ifndef WS_DEBUG_H
#define WS_DEBUG_H

#include <SDL.h>
#include <stdio.h>

#define ENABLE_DEBUG 1
#define DEBUG_AI

#if ENABLE_DEBUG
#define debug(format, ...) fprintf(stderr, format "\n", ##__VA_ARGS__)
#define drawDebugString(renderer, x, y, format, ...) \
    drawDebugStringReal(renderer, x, y, format, ##__VA_ARGS__)
#else
#define debug(format, ...)
#define drawDebugString(renderer, x, y, format, ...)
#endif

#ifdef DEBUG_CONTACTS
#define debug_contacts(...) debug(__VA_ARGS__)
#else
#define debug_contacts(...)
#endif

#ifdef DEBUG_AI
#define debug_ai(...) debug(__VA_ARGS__)
#else
#define debug_ai(...)
#endif

void drawDebugStringReal(SDL_Renderer *renderer, int x, int y,
                         const char *format, ...);

#endif // WS_DEBUG_H
