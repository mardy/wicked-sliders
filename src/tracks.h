#ifndef WS_TRACKS_H
#define WS_TRACKS_H

#include <cstddef>

struct TrackFile {
    const char *name; // NULL if read from track
    const char *data;
    size_t size;
};

namespace Tracks {

extern const TrackFile embeddedTracks[];
extern int embeddedTracksCount;

} // namespace

#endif // WS_TRACKS_H
