#ifndef WS_INPUT_DEVICE_H
#define WS_INPUT_DEVICE_H

#include <climits>
#include <cstdint>
#include <string>

enum class Buttons {
    OK = 0,
    Back,
};

class InputDevice {
public:
    float xAxis() const { return m_x / s_axisRange; }
    float yAxis() const { return m_y / s_axisRange; }
    int buttonCount() const { return m_buttons; }
    int axesCount() const { return m_axes; }
    int hatsCount() const { return m_hats; }
    bool isUsable() const { return m_isUsable; }

    virtual bool update() = 0;
    virtual bool buttonIsPressed(int index) = 0;
    virtual std::string name() const = 0;

    bool buttonIsPressed(Buttons button) {
        return buttonIsPressed(static_cast<int>(button));
    }

protected:
    static constexpr float s_axisRange = SHRT_MAX;
    int m_x;
    int m_y;
    int8_t m_buttons;
    int8_t m_axes;
    int8_t m_hats;
    bool m_isUsable = true;
};

#endif // WS_INPUT_DEVICE_H
