#ifndef WS_COLOR_H
#define WS_COLOR_H

#include <cstdint>

struct RgbColor {
    uint8_t r;
    uint8_t g;
    uint8_t b;
} __attribute__((packed));

#endif // WS_COLOR_H
