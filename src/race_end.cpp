#include "race_end.h"

#include "font_renderer.h"
#include "game_data.h"
#include "input_device.h"
#include "player.h"
#include "racer.h"
#include "ui_button.h"

#include <SDL2_gfxPrimitives.h>
#include <string>

#define PANEL_WIDTH 512
#define PANEL_HEIGHT 320

class RaceEndPrivate {
public:
    RaceEndPrivate(FontRenderer *playerFont, const GameData &gameData,
                   Player *leadPlayer);
    ~RaceEndPrivate();

    void renderTitle(SDL_Renderer *renderer);
    void renderRanking(SDL_Renderer *renderer);
    void prepareSurface(SDL_Renderer *renderer);

private:
    friend class RaceEnd;
    FontRenderer &m_playerFont;
    const GameData &m_gameData;
    Player *m_leadPlayer;
    SDL_Texture *m_panelTex;
    UIButton m_doneButton;
    bool m_isDone;
};

RaceEndPrivate::RaceEndPrivate(FontRenderer *playerFont,
                               const GameData &gameData,
                               Player *leadPlayer):
    m_playerFont(*playerFont),
    m_gameData(gameData),
    m_leadPlayer(leadPlayer),
    m_panelTex(nullptr),
    m_doneButton("OK", PANEL_WIDTH / 2, 50),
    m_isDone(false)
{
    m_doneButton.setFocused(true);
}

RaceEndPrivate::~RaceEndPrivate()
{
    if (m_panelTex) {
        SDL_DestroyTexture(m_panelTex);
    }
}

void RaceEndPrivate::renderTitle(SDL_Renderer *renderer)
{
    FontRenderer title(Fonts::Type::Title, 32);
    static const RgbColor titleColors[] = {
        { 255, 64, 64 },
        { 255, 255, 255 },
        { 64, 255, 64 },
    };
    const int numTitleColors = sizeof(titleColors) / sizeof(titleColors[0]);
    std::string titleText = "Ranking";

    const auto titleSize = title.size(titleText.c_str());
    SDL_Rect destRect {
        (PANEL_WIDTH - titleSize.first) / 2, 20,
        0, 0
    };
    for (int i = 0; i < titleText.length(); i++) {
        char text[2];
        text[0] = titleText[i];
        text[1] = 0;
        title.setColor(titleColors[i % numTitleColors]);
        SDL_Surface *s = title.renderText(text);
        SDL_Texture *t = SDL_CreateTextureFromSurface(renderer, s);
        destRect.w = s->w;
        destRect.y = s->h;
        SDL_RenderCopy(renderer, t, nullptr, &destRect);
        destRect.x += s->w;
        SDL_FreeSurface(s);
        SDL_DestroyTexture(t);
    }
}

void RaceEndPrivate::renderRanking(SDL_Renderer *renderer)
{
    const GameData &gd = m_gameData;

    int numPlayers = gd.racers.size();
    int numColumns = numPlayers > 6 ? 2 : 1;

    const int panelBorder = 20;
    int columnWidth = (PANEL_WIDTH - panelBorder * 2) / numColumns;
    int columnSpacing = 8;
    int rowSpacing = 4;
    int numPlayersPerColumn = MAX_PLAYERS / 2;

    uint16_t playerBoxWidth = columnWidth - columnSpacing;
    uint16_t playerBoxHeight = 28;
    for (const GameData::RacerData &rd: gd.racers) {
        int x = panelBorder +
            columnWidth * (rd.ranking / numPlayersPerColumn) +
            columnSpacing / 2;
        int y = 80 + (playerBoxHeight + rowSpacing) *
            (rd.ranking % numPlayersPerColumn);
        SDL_Rect boxRect {
            x, y,
            playerBoxWidth, playerBoxHeight
        };
        RgbColor c = rd.racer->color();
        SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, 255);
        SDL_RenderFillRect(renderer, &boxRect);
        SDL_Surface *s = m_playerFont.render("%d - %s - %.2f", rd.ranking + 1,
                                             rd.racer->name().c_str(),
                                             rd.times.back() / 1000.0);
        boxRect.x += 4;
        boxRect.y += 4;
        boxRect.w = s->w;
        boxRect.h = s->h;
        SDL_Texture *t = SDL_CreateTextureFromSurface(renderer, s);
        SDL_FreeSurface(s);
        SDL_RenderCopy(renderer, t, nullptr, &boxRect);
        SDL_DestroyTexture(t);
    }
}

void RaceEndPrivate::prepareSurface(SDL_Renderer *renderer)
{
    SDL_RendererInfo info;

    SDL_GetRendererInfo(renderer, &info);
    Uint32 format = SDL_PIXELFORMAT_RGBA4444;
    int bpp = 64; // arbitrarily high value
    for (int i = 0; i < info.num_texture_formats; i++) {
        Uint32 f = info.texture_formats[i];
        if (SDL_ISPIXELFORMAT_ALPHA(f) && SDL_BITSPERPIXEL(f) < bpp) {
            format = f;
            bpp = SDL_BITSPERPIXEL(f);
        }
    }

    int w = PANEL_WIDTH;
    int h = PANEL_HEIGHT;
    m_panelTex = SDL_CreateTexture(renderer, format, SDL_TEXTUREACCESS_TARGET,
                                   w, h);
    SDL_SetTextureBlendMode(m_panelTex, SDL_BLENDMODE_BLEND);
    SDL_SetRenderTarget(renderer, m_panelTex);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
    roundedBoxRGBA(renderer,
                   0, 0, w, h,
                   20, 64, 0, 255, 128);
    roundedRectangleRGBA(renderer,
                         0, 0, w - 1, h - 1,
                         20, 128, 0, 255, 255);
    renderTitle(renderer);
    renderRanking(renderer);
    SDL_SetRenderTarget(renderer, nullptr);
}

RaceEnd::RaceEnd(FontRenderer *playerFont, const GameData &gameData,
                 Player *leadPlayer):
    impl(new RaceEndPrivate(playerFont, gameData, leadPlayer))
{
}

RaceEnd::~RaceEnd() = default;

bool RaceEnd::isDone() const
{
    return impl->m_isDone;
}

void RaceEnd::update(int elapsedMs)
{
    (void)elapsedMs;

    Player *player = impl->m_leadPlayer;
    if (player->inputDevice()->buttonIsPressed(Buttons::OK)) {
        impl->m_isDone = true;
    }
}

void RaceEnd::blit(SDL_Renderer *dest, int off_x, int off_y)
{
    if (!impl->m_panelTex) {
        impl->prepareSurface(dest);
    }

    int screenWidth, screenHeight;
    SDL_GetRendererOutputSize(dest, &screenWidth, &screenHeight);
    SDL_Rect destRect {
        off_x + (screenWidth - PANEL_WIDTH) / 2,
        off_y + (screenHeight - PANEL_HEIGHT) / 2,
        PANEL_WIDTH, PANEL_HEIGHT
    };
    SDL_RenderCopy(dest, impl->m_panelTex, nullptr, &destRect);

    impl->m_doneButton.blit(dest,
                            destRect.x + PANEL_WIDTH / 4,
                            destRect.y + PANEL_HEIGHT - 50 - 10);
}
