#include "debug.h"

#include "font_renderer.h"

static FontRenderer *debugFont = nullptr;

static void drawDebugStringFixed(SDL_Renderer *renderer, int x, int y,
                                 const char *text)
{
    if (!debugFont) {
        debugFont = new FontRenderer(Fonts::Type::Text, 12);
    }

    uint8_t a;
    RgbColor color;
    SDL_GetRenderDrawColor(renderer, &color.r, &color.g, &color.b, &a);
    debugFont->setColor(color);

    debugFont->blitText(renderer, x, y, text);
}

void drawDebugStringReal(SDL_Renderer *renderer, int x, int y,
                         const char *format, ...)
{
    char buffer[512];
    va_list args;

    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer) - 1, format, args);
    va_end(args);
    buffer[sizeof(buffer) - 1] = 0;

    drawDebugStringFixed(renderer, x, y, buffer);
}
