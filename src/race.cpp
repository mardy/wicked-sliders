#include "race.h"

#include "box2d_glue.h"
#include "car_data.h"
#include "car_model.h"
#include "debug.h"
#include "font_renderer.h"
#include "game_data.h"
#include "geometry.h"
#include "player.h"
#include "race_end.h"
#include "race_panel.h"
#include "racer.h"
#include "timer.h"
#include "track.h"
#include "units.h"

#include <algorithm>
#include <b2_body.h>
#include <b2_chain_shape.h>
#include <b2_contact.h>
#include <b2_world.h>
#include <cmath>

static bool onNormalSize(const b2Fixture *lineFixture, const b2Vec2 &p)
{
    const b2ChainShape *shape = static_cast<const b2ChainShape*>(lineFixture->GetShape());
    return orientation(shape->m_vertices[0], shape->m_vertices[1], p) ==
        Orientation::CounterClockWise;
}

class RacePrivate: public b2ContactListener {
public:
    RacePrivate();

    void updateRacerTerrain(GameData::RacerData &racerData);
    void setRacerLevel(GameData::RacerData &racerData, CarLevel level);
    void raceEnded();

    void BeginContact(b2Contact* contact) override;
    void EndContact(b2Contact* contact) override;

    void handleHoleFall(GameData::RacerData &racerData);
    void updateRankings();

private:
    friend class Race;
    std::vector<Player*> m_players;
    std::unique_ptr<RaceEnd> m_raceEnd;
    GameData m_gameData;
    RacePanel m_panel;
    b2World m_world;
    bool m_isDone;
};

RacePrivate::RacePrivate():
    m_world({0.0, 0.0}),
    m_isDone(false)
{
    m_world.SetContactListener(this);
    m_gameData.lapCount = 1; // tmp
}

void RacePrivate::updateRacerTerrain(GameData::RacerData &racerData)
{
    Racer *racer = racerData.racer;
    float linearDamping = racer->carData().params.linearDamping;
    float angularDamping = racer->carData().params.angularDamping;

    b2Body *body = racer->body();
    for (int i = 0; i < racerData.terrainCount; i++) {
        Track::ObjectInfo info = {};
        bool found = m_gameData.track->getInfo(racerData.terrains[i], &info);
        if (!found) return;

        if (info.flags & Track::ObjectInfo::AffectsDamping) {
            debug("Updating damping!");
            linearDamping = info.linearDamping;
            angularDamping = info.angularDamping;
        }
    }
    debug("Setting dampings to %.2f and %.2f", linearDamping, angularDamping);
    body->SetLinearDamping(linearDamping);
    body->SetAngularDamping(angularDamping);

    racerData.terrainsChanged = false;
}

void RacePrivate::setRacerLevel(GameData::RacerData &racerData,
                                CarLevel level)
{
    racerData.racer->setLevel(level);
}

void RacePrivate::raceEnded()
{
    fprintf(stderr, "Race ended\n");
    m_raceEnd.reset(new RaceEnd(&m_panel.fontRenderer(), m_gameData,
                                m_players.front()));
}

void RacePrivate::BeginContact(b2Contact* contact)
{
    const b2Contact *c = contact;
    const b2Fixture *fixtureA = c->GetFixtureA();
    b2Fixture *fixtureB = contact->GetFixtureB();
    UserData ud0(fixtureA);
    UserData ud1(fixtureB);
    BodyData &bd0 = ud0.data;
    BodyData &bd1 = ud1.data;
    debug_contacts("Begin contact %p, A=%d, B=%d", c,
                   int(ud0.data.bodyType),
                   int(ud1.data.bodyType));
    // We only care about racer contacts
    if (bd0.bodyType != BodyType::Racer && bd1.bodyType != BodyType::Racer) {
        return;
    }

    if (bd0.bodyType == BodyType::TrackElement) {
        GameData::RacerData &racerData = m_gameData.racers[bd1.index];
        if (racerData.terrainCount ==
            sizeof(racerData.terrains) / sizeof(racerData.terrains[0])) {
            fprintf(stderr, "Terrain count exhausted!\n");
            return;
        }
        racerData.terrains[racerData.terrainCount++] = bd0.index;
        racerData.terrainsChanged = true;
    } else if (bd0.bodyType == BodyType::CheckPoint) {
        debug_contacts("Player %d entered checkpoint %d",
                       bd1.index, bd0.index);
        GameData::RacerData &racerData = m_gameData.racers[bd1.index];
        if (racerData.nextCheckPoint == bd0.index) {
            racerData.nextCheckPoint++;
        }
    } else if (bd0.bodyType == BodyType::ComputerPoint) {
        debug_contacts("Player %d entered computer point %d",
                       bd1.index, bd0.index);
        GameData::RacerData &racerData = m_gameData.racers[bd1.index];
        if (racerData.nextComputerPoint == bd0.index) {
            if (racerData.nextComputerPoint + 1 < m_gameData.computerPoints.size()) {
                racerData.nextComputerPoint++;
            } else {
                racerData.nextComputerPoint = 0;
            }
        }
    } else if (bd0.bodyType == BodyType::FinishLine) {
        GameData::RacerData &racerData = m_gameData.racers[bd1.index];
        if (racerData.lap < m_gameData.lapCount &&
            racerData.nextCheckPoint == m_gameData.checkPointCount) {
            racerData.nextCheckPoint = 0;
            racerData.lap++;
            racerData.times.push_back(m_gameData.currentTimeMs);
            fprintf(stderr, "Player %d completed lap %d in %d ms\n",
                    bd1.index, racerData.lap, racerData.times.back());
            if (racerData.lap == m_gameData.lapCount) {
                // TODO: convert human racers to AI
                m_gameData.racersDone++;
                if (m_gameData.racersDone == m_gameData.racers.size()) {
                    raceEnded();
                }
            }
        } else {
            debug_contacts("Player %d crossed with last checkpoint %d",
                           bd1.index, racerData.nextCheckPoint);
        }
    } else if (bd0.bodyType == BodyType::RampFloor ||
               bd0.bodyType == BodyType::RampUpper) {
        GameData::RacerData &racerData = m_gameData.racers[bd1.index];
        CarData &carData = racerData.racer->carData();

        bool isOnNormalSide = onNormalSize(fixtureA, {p2m(carData.x), p2m(carData.y)});

        debug_contacts("Player %d entered ramp %d (%s) from %s side (floors: %d, uppers: %d)",
                       bd1.index, bd0.index,
                       bd0.bodyType == BodyType::RampFloor ? "floor" : "upper",
                       isOnNormalSide ? "floor" : "upper",
                       racerData.rampFloorsEntered, racerData.rampUppersEntered);

        if (bd0.bodyType == BodyType::RampFloor) {
            racerData.rampFloorsEntered++;
            if (racerData.rampFloorsEntered > 1) return;
            if (isOnNormalSide || racerData.rampUppersEntered == 0) {
                setRacerLevel(racerData, CarLevel::RampUpwards);
            }
        } else if (bd0.bodyType == BodyType::RampUpper) {
            racerData.rampUppersEntered++;
            if (racerData.rampUppersEntered > 1) return;
            if (!isOnNormalSide || racerData.rampFloorsEntered == 0) {
                setRacerLevel(racerData, CarLevel::RampDownwards);
            }
        }
    } else if (bd0.bodyType == BodyType::Hole) {
        GameData::RacerData &racerData = m_gameData.racers[bd1.index];
        racerData.holesEntered++;
    }
}

void RacePrivate::EndContact(b2Contact* contact)
{
    const b2Contact *c = contact;
    const b2Fixture *fixtureA = c->GetFixtureA();
    b2Fixture *fixtureB = contact->GetFixtureB();
    UserData ud0(fixtureA);
    UserData ud1(fixtureB);
    BodyData &bd0 = ud0.data;
    BodyData &bd1 = ud1.data;
    debug_contacts("End contact %p, A=%d, B=%d", c,
                   int(ud0.data.bodyType),
                   int(ud1.data.bodyType));

    // We only care about racer contacts
    if (bd0.bodyType != BodyType::Racer && bd1.bodyType != BodyType::Racer) {
        return;
    }

    if (bd0.bodyType == BodyType::TrackElement) {
        GameData::RacerData &racerData = m_gameData.racers[bd1.index];
        int targetIndex = -1;
        for (int i = 0; i < racerData.terrainCount; i++) {
            if (targetIndex == -1) {
                if (racerData.terrains[i] == bd0.index) {
                    targetIndex = i;
                }
            } else {
                racerData.terrains[targetIndex++] = racerData.terrains[i];
            }
        }
        if (targetIndex == -1) {
            fprintf(stderr, "Missing terrain contact!\n");
            return;
        }
        racerData.terrainCount--;
        racerData.terrainsChanged = true;
    } else if (bd0.bodyType == BodyType::RampFloor ||
               bd0.bodyType == BodyType::RampUpper) {
        GameData::RacerData &racerData = m_gameData.racers[bd1.index];
        CarData &carData = racerData.racer->carData();

        bool isOnNormalSide = onNormalSize(fixtureA, {p2m(carData.x), p2m(carData.y)});
        debug_contacts("Player %d left ramp %d (%s) from %s side (floors: %d, uppers: %d)",
                       bd1.index, bd0.index,
                       bd0.bodyType == BodyType::RampFloor ? "floor" : "upper",
                       isOnNormalSide ? "floor" : "upper",
                       racerData.rampFloorsEntered, racerData.rampUppersEntered);

        /* If the car exits the ramp while on a hybrid level, we must revert
         * the level change.
         * If it exits on the floor or on the upper level, we must remove the
         * Ramp collision bit. */
        if (bd0.bodyType == BodyType::RampFloor) {
            racerData.rampFloorsEntered--;
            if (racerData.rampUppersEntered > 0) return;
            if (isOnNormalSide) {
                setRacerLevel(racerData, CarLevel::Floor);
            } else {
                // We were going down, but went back
                setRacerLevel(racerData, CarLevel::RampDownwards);
            }
        } else if (bd0.bodyType == BodyType::RampUpper) {
            racerData.rampUppersEntered--;
            if (racerData.rampUppersEntered > 0) return;
            if (!isOnNormalSide) {
                setRacerLevel(racerData, CarLevel::Upper);
            } else {
                // We were going up, but when back
                setRacerLevel(racerData, CarLevel::RampUpwards);
            }
        }
    } else if (bd0.bodyType == BodyType::Hole) {
        GameData::RacerData &racerData = m_gameData.racers[bd1.index];
        if (racerData.holesEntered > 0) {
            racerData.holesEntered--;
        }
    }
}

void RacePrivate::handleHoleFall(GameData::RacerData &racerData)
{
    Racer *racer = racerData.racer;
    b2Body *body = racer->body();

    for (b2ContactEdge *edge = body->GetContactList(); edge; edge = edge->next) {
        b2Fixture *fixture = edge->contact->GetFixtureA();
        UserData ud(fixture);
        BodyData &bd = ud.data;
        if (bd.bodyType == BodyType::Hole) {
            CarData &carData = racer->carData();
            b2Vec2 testPoints[4] = {
                { p2m(carData.x + 2), p2m(carData.y + 2) },
                { p2m(carData.x + 2), p2m(carData.y - 2) },
                { p2m(carData.x - 2), p2m(carData.y + 2) },
                { p2m(carData.x - 2), p2m(carData.y - 2) },
            };
            for (int i = 0; i < 4; i++) {
                if (fixture->TestPoint(testPoints[i])) {
                    /* We have fallen! */
                    b2Filter filter;
                    carData.level = CarLevel::Floor;
                    filter.categoryBits = uint16_t(CollisionCategory::Floor);
                    edge->contact->GetFixtureB()->SetFilterData(filter);
                    return;
                }
            }
        }
    }
}

void RacePrivate::updateRankings()
{
    int numPlayers = m_gameData.racers.size();
    std::vector<int> positions(numPlayers);
    for (int i = 0; i < numPlayers; i++) {
        positions[i] = i;
    }

    std::sort(positions.begin(), positions.end(), [this](int p0, int p1) {
        const GameData::RacerData &racer0 = m_gameData.racers[p0];
        const GameData::RacerData &racer1 = m_gameData.racers[p1];
        if (racer0.lap != racer1.lap) {
            return racer0.lap > racer1.lap;
        }
        if (racer0.lap == m_gameData.lapCount) {
            /* Both have already finished. Check who was first */
            return racer0.times.back() < racer1.times.back();
        }
        if (racer0.nextCheckPoint != racer1.nextCheckPoint) {
            return racer0.nextCheckPoint > racer1.nextCheckPoint;
        }
        return racer0.checkPointDistance2 < racer1.checkPointDistance2;
    });

    for (int i = 0; i < numPlayers; i++) {
        m_gameData.racers[positions[i]].ranking = i;
    }
}

Race::Race():
    impl(new RacePrivate())
{
}

Race::~Race() = default;

void Race::setTrack(Track *track)
{
    impl->m_gameData.track = track;
    impl->m_gameData.checkPointCount = track->checkPointCount();
    impl->m_gameData.computerPoints = track->computerPoints();
    track->populateWorld(impl->m_world);
}

void Race::setLapCount(int count)
{
    impl->m_gameData.lapCount = count;
}

GameData &Race::gameData()
{
    return impl->m_gameData;
}

const GameData &Race::gameData() const
{
    return impl->m_gameData;
}

void Race::addPlayer(Player *player)
{
    int count = impl->m_players.size();

    impl->m_players.push_back(player);
    Racer *racer = player->makeRacer();
    impl->m_gameData.racers.push_back({
        racer,
        0, // lap
        0, // nextCheckPoint
        0, // nextComputerPoint
        int8_t(count), // ranking
        0, // rampFloorsEntered
        0, // rampUppersEntered
        0, // holesEntered
        false, // terrainsChanged
        0, // checkPointDistance2
        0, // terrainCount
    });

    RacerPlacement placement;
    impl->m_gameData.track->placeRacer(count, &placement);
    CarData &d = racer->carData();
    d.x = placement.x; d.y = placement.y;
    d.angle = placement.angle;

    racer->populateWorld(impl->m_world, count);
    impl->setRacerLevel(impl->m_gameData.racers.back(), CarLevel::Floor);
}

void Race::update(Timer *timer)
{
    float elapsedSeconds = timer->elapsedSecs();

    GameData &gd = impl->m_gameData;

    /* TODO: handle Starting state */
    if (gd.state == GameData::State::None) {
        gd.state = GameData::State::Started;
        gd.startTimeMs = timer->timeMs();
    }
    gd.currentTimeMs = timer->timeMs() - gd.startTimeMs;

    for (GameData::RacerData &racerData: gd.racers) {
        Racer *racer = racerData.racer;
        racer->updateInput(gd);

        if (racerData.terrainsChanged) {
            impl->updateRacerTerrain(racerData);
        }
        CarData &d = racer->carData();

        float inputTime = elapsedSeconds;
        float rotationFactor = inputTime * 5;
        float accelerationFactor = inputTime * 8000;

        b2Body *body = racer->body();
        b2Vec2 forwardNormal = body->GetWorldVector({0, 1});
        b2Vec2 accelerationNormal = forwardNormal;
        b2Vec2 force = d.acceleration * accelerationFactor * accelerationNormal;
        body->ApplyForce(force, body->GetWorldCenter(), true);
        if (d.steer != 0) {
            /* We don't use ApplyTorque(), because we'd need to counter its
             * prolonged effect. */
            float angle = fmodf(body->GetAngle() + d.steer * rotationFactor, 2 * M_PI);
            body->SetTransform(body->GetPosition(), angle);
        }

        if (racerData.holesEntered > 0) {
            impl->handleHoleFall(racerData);
        }
    }

    impl->m_world.Step(elapsedSeconds, 5, 5);

    for (GameData::RacerData &racerData: gd.racers) {
        Racer *racer = racerData.racer;
        CarData &d = racer->carData();
        b2Vec2 center = racer->body()->GetPosition();
        d.x = m2p(center.x);
        d.y = m2p(center.y);
        d.angle = -racer->body()->GetAngle();

        /* Update squared distance to next checkpoint (needed for player
         * ranking). */
        racerData.checkPointDistance2 =
            gd.track->distance2ToCheckPoint(d.x, d.y,
                                            racerData.nextCheckPoint);
    }

    impl->updateRankings();
    impl->m_panel.update(gd, timer->elapsedMs());

    if (impl->m_raceEnd) {
        impl->m_raceEnd->update(timer->elapsedMs());
    }
}

bool Race::isDone() const
{
    return impl->m_raceEnd ? impl->m_raceEnd->isDone() : impl->m_isDone;
}

void Race::blit(SDL_Renderer *dest, int off_x, int off_y)
{
    GameData &gd = impl->m_gameData;

    gd.track->blit(dest, off_x, off_y, nullptr);

    for (GameData::RacerData &racerData: gd.racers) {
        Racer *racer = racerData.racer;
        if (racer->carData().level == CarLevel::Floor) {
            racer->blit(dest, off_x, off_y);
        }
    }

    gd.track->blitUpper(dest, off_x, off_y, nullptr);

    for (GameData::RacerData &racerData: gd.racers) {
        Racer *racer = racerData.racer;
        if (racer->carData().level != CarLevel::Floor) {
            racer->blit(dest, off_x, off_y);
        }
    }

    int trackHeight = 368; // TODO: get it from track
    impl->m_panel.blit(dest, off_x, off_y + trackHeight, gd);

    if (impl->m_raceEnd) {
        impl->m_raceEnd->blit(dest, off_x, off_y);
    }
}
