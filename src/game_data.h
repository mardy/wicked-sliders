#ifndef WS_GAME_DATA_H
#define WS_GAME_DATA_H

#include "geometry.h"

#include <vector>

#define MAX_PLAYERS 12

class Racer;
class Track;

struct GameData {
    enum class State {
        None = 0,
        Starting,
        Started,
        Ended,
    };

    struct RacerData {
        Racer* racer;
        int8_t lap;
        int8_t nextCheckPoint;
        int8_t nextComputerPoint;
        int8_t ranking;
        int8_t rampFloorsEntered;
        int8_t rampUppersEntered;
        int8_t holesEntered;
        bool terrainsChanged;
        int checkPointDistance2;
        int terrainCount;
        int16_t terrains[4];
        std::vector<uint32_t> times;
    };
    Track *track;
    std::vector<RacerData> racers;
    std::vector<Point> computerPoints;
    int8_t racersDone = 0;
    int checkPointCount;
    int lapCount;
    State state = State::None;
    uint32_t startTimeMs;
    uint32_t currentTimeMs;
};

#endif // WS_GAME_DATA_H
