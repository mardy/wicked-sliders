#ifndef WS_TRACK_LOADER_H
#define WS_TRACK_LOADER_H

#include <cstddef>

class Track;

class TrackLoader {
public:
    static Track *loadTrack(const char *data, size_t size);

protected:
    virtual bool checkFormat(const char *data, size_t size) const = 0;
    virtual Track *load(const char *data, size_t size) = 0;
};

#endif // WS_TRACK_LOADER_H
