#include "car_model_factory.h"
#include "font_renderer.h"
#include "game_data.h"
#include "input_device.h"
#include "joystick_input.h"
#include "keyboard_input.h"
#include "player.h"
#include "race.h"
#include "racer.h"
#include "timer.h"
#include "track.h"
#include "track_loader.h"
#include "tracks.h"

// normal includes
#include <stdlib.h>
#include <time.h>
#ifdef NINTENDO_WII
#include <gccore.h>
#include <wiiuse/wpad.h>
#endif

// SDL includes
#include <SDL.h>
#include <SDL2_framerate.h>
#include <SDL_ttf.h>

#include <algorithm>
#include <climits>
#include <cmath>
#include <string>
#include <vector>

extern char _binary_images_data_end[], _binary_images_data_start[];

SDL_Renderer *renderer = NULL;

void init() {

    setenv("SDL_WII_JOYSTICK_SPLIT", "1", 1);
    //setenv("SDL_OGC_DEBUG_ACCEL", "1", 1);
    //setenv("SDL_OGC_DISABLE_ACCEL", "1", 1);
    // initialize SDL video. If there was an error SDL shows it on the screen
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK) < 0) {
        fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
        SDL_Delay(5000);
        exit(EXIT_FAILURE);
    }

    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_INFO);

    // make sure SDL cleans up before exit
    atexit(SDL_Quit);
    SDL_ShowCursor(SDL_DISABLE);

    // create a new window
    SDL_Window *window = SDL_CreateWindow("Wicked Sliders",
                                          SDL_WINDOWPOS_UNDEFINED,
                                          SDL_WINDOWPOS_UNDEFINED,
                                          640, 480, 0);
    if (!window) {
        fprintf(stderr, "Unable to set video: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    renderer = SDL_CreateRenderer(window, -1, 0);

    TTF_Init();
}

void cleanup(){

    // we have to quit SDL
    SDL_Quit();
    exit(EXIT_SUCCESS);
}

int main(int argc, char** argv){
    // main function. Always starts first

    // to stop the while loop
    bool done = false;

    // start init() function
    init();

    std::vector<Player> players;

    struct PlayerPreset {
        RgbColor color;
    };
    static const std::vector<PlayerPreset> player_presets = {
        {{ 255, 0, 0 }},
        {{ 0, 255, 0 }},
        {{ 0, 0, 255 }},
        {{ 255, 255, 0 }},
        {{ 0, 255, 255 }},
        {{ 255, 0, 255 }},

        {{ 255, 128, 0 }},
        {{ 0, 255, 128 }},
        {{ 255, 0, 128 }},
        {{ 128, 255, 0 }},
        {{ 0, 128, 255 }},
        {{ 128, 0, 255 }},

        {{ 255, 128, 255 }},
        {{ 255, 255, 128 }},
        {{ 255, 255, 128 }},
        {{ 128, 255, 255 }},
        {{ 255, 128, 255 }},
        {{ 128, 255, 255 }},
    };
    int numPlayers = 0;

    const CarModel &car = CarModelFactory::models[0];

    int numKeys = 0;
    const Uint8 *keyState = SDL_GetKeyboardState(&numKeys);
    if (numKeys > 0) {
        const PlayerPreset &p = player_presets[numPlayers++];
        auto &player =
            players.emplace_back(new KeyboardInput(keyState));
        player.setName("Player " + std::to_string(players.size() - 1));
        player.setColor(p.color);
        player.setCarModel(car);
    }

#ifdef NINTENDO_WII
    while (SDL_NumJoysticks() == 0) {
        SDL_PumpEvents();
    }
#endif

    for (int i = 0; i < SDL_NumJoysticks(); i++) {
        SDL_Joystick *joystick = SDL_JoystickOpen(i);
        if (!joystick) continue;

        JoystickInput *joyInput = new JoystickInput(joystick);
        if (!joyInput->isUsable()) continue;

        const PlayerPreset &p = player_presets[numPlayers++];
        auto &player =
            players.emplace_back(joyInput);
        player.setName("Player " + std::to_string(players.size() - 1));
        player.setColor(p.color);
        player.setCarModel(car);
    }

    for (int i = players.size(); i < 4; i++) {
        const PlayerPreset &p = player_presets[numPlayers++];
        auto &player = players.emplace_back(nullptr);
        player.setName("CPU " + std::to_string(players.size() - 1));
        player.setColor(p.color);
        player.setCarModel(car);
    }

    Race race;
    const TrackFile &firstTrack = Tracks::embeddedTracks[0];
    Track *track = TrackLoader::loadTrack(firstTrack.data, firstTrack.size);
    race.setTrack(track);

    for (Player &player: players) {
        race.addPlayer(&player);
    }

    Timer *timer = Timer::instance();

    uint32_t lastTick = timer->timeMs();
    const int desiredFPS = 50;

    FontRenderer font(Fonts::Type::Text, 20);
    font.setColor({255, 255, 0});

    int frames = 0;
    float fps = 0.0;
    uint32_t secondTick = lastTick;
    FPSmanager frameRateManager;
    SDL_initFramerate(&frameRateManager);
    SDL_setFramerate(&frameRateManager, desiredFPS);
    // this is the endless while loop until done = true
    while (!done) {
        SDL_PumpEvents();
        SDL_JoystickUpdate();

        if (keyState[SDL_SCANCODE_ESCAPE]) done = true;

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
#ifdef NINTENDO_WII
        // scans if a button was pressed
        WPAD_ScanPads();
        u32 held = WPAD_ButtonsHeld(0);

        // if the homebutton is pressed it will set done = true and it will fill the screen
        // with a black background
        if(held & WPAD_BUTTON_HOME){
            done=true;
        }
#endif

        timer->frameStarted(SDL_GetTicks());
        SDL_RenderClear(renderer);

        if (race.isDone()) {
            // TODO: move to another screen
            done = true;
        }

        race.update(timer);
        race.blit(renderer, 0, 0);
        font.blit(renderer, 300, 460, "FPS %.1f", fps);

        SDL_RenderPresent(renderer);
        frames++;

        SDL_framerateDelay(&frameRateManager);
        if (timer->timeMs() - secondTick >= 1000) {
            fps = float(frames) * 1000 / (timer->timeMs() - secondTick);
            secondTick = timer->timeMs();
            frames = 0;
        }
    }

    cleanup();

    return 0;
}
