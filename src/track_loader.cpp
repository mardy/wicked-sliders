#include "track_loader.h"

#include "track_loader_ss.h"

#include <memory>

Track *TrackLoader::loadTrack(const char *data, size_t size)
{
    std::unique_ptr<TrackLoader> loader;

    loader.reset(new TrackLoaderSS);
    if (loader->checkFormat(data, size)) {
        return loader->load(data, size);
    }

    return nullptr;
}
