#ifndef WS_POINT_ANIMATOR_H
#define WS_POINT_ANIMATOR_H

#include <cstdint>
#include <utility>
#include <vector>

class PointAnimator {
public:
    struct Point {
        int16_t x;
        int16_t y;
        bool operator==(const Point &o) { return x == o.x && y == o.y; }
    };

    using Points = std::vector<Point>;
    void setPoints(Points points);
    void setSpeed(int pixelsPerSecond);

    void setElapsed(int elapsed);
    void reorder(int source, int target);

    const Points &currentPositions() const { return m_currentPositions; }

private:
    int m_pixelsPerSecond = 100;
    Points m_points;
    std::vector<std::pair<int, int>> m_targets; // index, timestamp
    Points m_currentPositions;
};

#endif // WS_POINT_ANIMATOR_H
