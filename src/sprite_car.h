#ifndef WS_SPRITE_CAR_H
#define WS_SPRITE_CAR_H

#include "color.h"

#include <SDL.h>
#include <stdint.h>

struct EmbeddedImage;

class SpriteCar {
public:
    SpriteCar(SDL_Renderer *renderer,
              const EmbeddedImage *imgColor,
              const EmbeddedImage *imgGrey,
              RgbColor color);
    virtual ~SpriteCar();

    void blit(SDL_Renderer *dest, int dest_x, int dest_y, float angle);

private:
    SDL_Texture *m_texture;
};

#endif // WS_SPRITE_CAR_H
