#include "point_animator.h"

#include <cmath>
#include <cstdio>

static void interpolate(PointAnimator::Point &a, const PointAnimator::Point &b,
                        float pixelSpeed)
{
    float dx = b.x - a.x;
    float dy = b.y - a.y;
    float distance = std::sqrt(dx * dx + dy * dy);
    float ratio = pixelSpeed / distance;
    if (ratio >= 1.0) {
        a.x = b.x;
        a.y = b.y;
    } else {
        a.x += dx * ratio;
        a.y += dy * ratio;
    }
}

void PointAnimator::setPoints(Points points)
{
    m_points = std::move(points);
    m_currentPositions = m_points;

    m_targets.clear();
    for (int i = 0; i < m_points.size(); i++) {
        m_targets.emplace_back(std::make_pair(i, 0));
    }
}

void PointAnimator::setSpeed(int pixelsPerSecond)
{
    m_pixelsPerSecond = pixelsPerSecond;
}

void PointAnimator::setElapsed(int elapsed)
{
    int i = 0;
    float pixelSpeed = elapsed * m_pixelsPerSecond / 1000.0;
    for (const auto &target: m_targets) {
        Point &current = m_currentPositions[i++];
        const Point &t = m_points[target.first];
        if (current == t) continue;

        interpolate(current, t, pixelSpeed);
    }
}

void PointAnimator::reorder(int source, int target)
{
    m_targets[source] = std::make_pair(target, 0);
}
