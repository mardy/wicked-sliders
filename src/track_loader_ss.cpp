#include "track_loader_ss.h"

#include "SDL_gfxBlitFunc.h"
#include "box2d_glue.h"
#include "debug.h"
#include "ss_tiles.h"
#include "track.h"
#include "units.h"

#include <SDL_endian.h>
#include <b2_body.h>
#include <b2_chain_shape.h>
#include <b2_circle_shape.h>
#include <b2_fixture.h>
#include <b2_polygon_shape.h>
#include <b2_world.h>
#include <vector>

struct Size {
    uint16_t w;
    uint16_t h;
};

struct Rectangle {
    uint16_t x0;
    uint16_t y0;
    uint16_t x1;
    uint16_t y1;
};

struct ComputerPoint {
    Rectangle area;
    Point target;
};

struct Object {
    uint16_t x;
    uint8_t y;
    uint8_t type;
    int8_t rotation;

    void size(Size *size) const;
    inline void unrotatedSize(Size *size) const;
};

struct StartPoint {
    int16_t x;
    int16_t y;
    float angle;
};

struct CachedTile {
    int16_t x;
    int16_t y;
    int8_t rotation;
    SDL_Texture *surface;
};

struct TileSurface {
    int16_t x;
    int16_t y;
    SDL_Surface *surface;
};

struct TrackSS: public Track {
    TrackSS();
    ~TrackSS();

    int objectIndex(const Object &o) const;
    void addRectFixture(b2Body *body, const Object &o, b2FixtureDef *def);
    void addCircleFixture(b2Body *body, const Object &o, b2FixtureDef *def);
    void addPolygonalFixture(b2Body *body, const Object &o,
                             const SSTileShapes &shapes);
    void initFixture(b2FixtureDef *def, TileFlag flags, int index) const;
    void addFixture(b2Body *body, const Object &o);

    void populateWorldCheckPoints(b2World &world) const;
    void populateWorldComputerPoints(b2World &world) const;

    void populateWorld(b2World &world) override;
    void blit(SDL_Renderer *dest, int dest_x, int dest_y,
              SDL_Rect *sourceRect) override;
    void blitUpper(SDL_Renderer *dest, int dest_x, int dest_y,
                   SDL_Rect *sourceRect) override;
    void placeRacer(int position, RacerPlacement *placement) override;
    bool getInfo(int objectIndex, ObjectInfo *objectInfo) const override;
    int checkPointCount() const override { return m_checkPoints.size(); }
    int distance2ToCheckPoint(int16_t x, int16_t y,
                              int checkPointIndex) const override;
    std::vector<Point> computerPoints() const override;

    void blitCheckPoints(SDL_Renderer *dest, int dest_x, int dest_y);
    void blitComputerPoints(SDL_Renderer *dest, int dest_x, int dest_y);

    static SDL_Surface *createTileSurface(const EmbeddedImage &tile);
    static SDL_Texture *createTileTexture(SDL_Renderer *renderer,
                                          const EmbeddedImage &tile);
    static SDL_Surface *rotatedTileSurface(const EmbeddedImage &tile,
                                           int rotation);
    static void copyAlphaToOverlappingTiles(SDL_Surface *source,
                                  const std::vector<TileSurface> &upperTiles,
                                  int16_t sourceX, int16_t sourceY);
    void renderBackground(SDL_Renderer *renderer);

    std::vector<Object> m_objects;
    std::vector<Rectangle> m_checkPoints;
    std::vector<ComputerPoint> m_computerPoints;
    StartPoint m_startPoint;
    b2Body *m_groundBody;
    SDL_Texture *m_trackBackground;
    std::vector<CachedTile> m_upperTiles;
};

static inline void rotate90(int *x, int *y, const Size &size, int rotation)
{
    if (rotation == 1) {
        int tmp = *y;
        *y = *x;
        *x = size.h - tmp;
    } else if (rotation == 2) {
        *x = size.w - *x;
        *y = size.h - *y;
    } else if (rotation == 3) {
        int tmp = *x;
        *x = *y;
        *y = size.w - tmp;
    }
}

static void drawTexture(SDL_Renderer *dest, SDL_Texture *tex,
                        int x, int y, int rotation)
{
    SDL_Rect destRect {
        x, y,
    };
    SDL_QueryTexture(tex, nullptr, nullptr, &destRect.w, &destRect.h);
    if (rotation == 1 || rotation == 3) {
        int diff = (destRect.w - destRect.h) / 2;
        destRect.x -= diff;
        destRect.y += diff;
    }
    SDL_RenderCopyEx(dest, tex, nullptr, &destRect,
                     rotation * 90, nullptr, SDL_FLIP_NONE);
}

void Object::size(Size *size) const
{
    const SSTile &tileData = ss_tiles[type];
    const EmbeddedImage *tile = tileData.image;

    if (rotation % 2 == 0) {
        size->w = tile->width;
        size->h = tile->height;
    } else {
        size->w = tile->height;
        size->h = tile->width;
    }
}

void Object::unrotatedSize(Size *size) const
{
    const SSTile &tileData = ss_tiles[type];
    const EmbeddedImage *tile = tileData.image;
    size->w = tile->width;
    size->h = tile->height;
}

TrackSS::TrackSS():
    m_trackBackground(nullptr)
{
}

TrackSS::~TrackSS()
{
    if (m_trackBackground) {
        SDL_DestroyTexture(m_trackBackground);
    }
    for (const CachedTile &tile: m_upperTiles) {
        SDL_DestroyTexture(tile.surface);
    }
}

int TrackSS::objectIndex(const Object &o) const
{
    return &o - &m_objects.front();
}

void TrackSS::addRectFixture(b2Body *body, const Object &o, b2FixtureDef *def)
{
    Size size;
    o.size(&size);

    b2PolygonShape shape;
    shape.SetAsBox(p2m(size.w / 2), p2m(size.h / 2),
                   { p2m(size.w / 2 + o.x * 2), p2m(size.h / 2 + o.y * 2) },
                   0.0);

    def->shape = &shape;
    body->CreateFixture(def);
}

void TrackSS::addCircleFixture(b2Body *body, const Object &o, b2FixtureDef *def)
{
    Size size;
    o.size(&size);

    b2CircleShape shape;
    shape.m_radius = p2m(size.w / 2);
    shape.m_p = { p2m(size.w / 2 + o.x * 2), p2m(size.h / 2 + o.y * 2) };

    def->shape = &shape;
    body->CreateFixture(def);
}

void TrackSS::addPolygonalFixture(b2Body *body, const Object &o,
                                  const SSTileShapes &shapes)
{
    Size size;
    o.unrotatedSize(&size);

    int index = objectIndex(o);
    for (int i = 0; i < shapes.numShapes; i++) {
        const SSTileShape &tileShape = shapes.shapes[i];
        b2Vec2 points[tileShape.numPoints];
        for (int j = 0; j < tileShape.numPoints; j++) {
            int x = tileShape.points[j * 2];
            int y = tileShape.points[j * 2 + 1];
            rotate90(&x, &y, size, o.rotation);
            points[j] = { p2m(x + o.x * 2), p2m(y + o.y * 2) };
        }
        b2FixtureDef def;
        initFixture(&def, tileShape.flags, index);
        if (tileShape.isClosed) {
            b2PolygonShape shape;
            shape.Set(points, tileShape.numPoints);
            def.shape = &shape;
            body->CreateFixture(&def);
        } else {
            b2ChainShape shape;
            // First and last points are ghost ones
            shape.CreateChain(points + 1, tileShape.numPoints - 2,
                              points[0], points[tileShape.numPoints - 1]);
            def.shape = &shape;
            body->CreateFixture(&def);
        }
    }
}

void TrackSS::initFixture(b2FixtureDef *def, TileFlag flags, int index) const
{
    UserData userData(BodyType::TrackElement, index);

    uint16_t mask = 0;
    uint16_t category = 0;

    if (flags & TileFlag::UpperOnly) {
        mask |= uint16_t(CollisionCategory::Upper);
        category |= uint16_t(CollisionCategory::Upper);
    } else if (flags & TileFlag::FloorOnly) {
        mask |= uint16_t(CollisionCategory::Floor);
        category |= uint16_t(CollisionCategory::Floor);
    } else {
        mask |= uint16_t(CollisionCategory::Floor | CollisionCategory::Upper);
        category |= uint16_t(CollisionCategory::Floor | CollisionCategory::Upper);
    }

    if (flags & TileFlag::Finish) {
        def->isSensor = true;
        userData.data.bodyType = BodyType::FinishLine;
    } else if (flags & TileFlag::Wall) {
        userData.data.bodyType = BodyType::Wall;
        if (flags & TileFlag::FloorOnly) {
            category = uint16_t(CollisionCategory::UnderBridgeWall);
        }
    } else if (flags & TileFlag::Grass) {
        def->isSensor = true;
        flags |= TileFlag::FloorOnly;
    } else if (flags & TileFlag::Ramp) {
        def->isSensor = true;
        userData.data.bodyType = flags & TileFlag::FloorOnly ?
            BodyType::RampFloor : BodyType::RampUpper;
        mask |= uint16_t(CollisionCategory::Ramp);
    } else if (flags & TileFlag::Hole) {
        def->isSensor = true;
        userData.data.bodyType = BodyType::Hole;
    }

    def->filter.maskBits = mask;
    def->filter.categoryBits = category;
    def->userData.pointer = userData.pointer;
}

void TrackSS::addFixture(b2Body *body, const Object &o)
{
    const SSTile &tileData = ss_tiles[o.type];
    if (tileData.flags & TileFlag::Rectangular) {
        b2FixtureDef def;
        initFixture(&def, tileData.flags, objectIndex(o));
        addRectFixture(body, o, &def);
    } else if (tileData.flags & TileFlag::Circular) {
        b2FixtureDef def;
        initFixture(&def, tileData.flags, objectIndex(o));
        addCircleFixture(body, o, &def);
    } else {
        const SSTileShapes *shapes = SSTiles::shapesForTile(o.type);
        if (!shapes) {
            fprintf(stderr, "Unsupported shape for item %d at %d,%d\n",
                    o.type, o.x * 2, o.y * 2);
            return;
        }

        addPolygonalFixture(body, o, *shapes);
    }
}

void TrackSS::populateWorldCheckPoints(b2World &world) const
{
    UserData userData(BodyType::CheckPoint, 0);

    b2PolygonShape shape;
    b2FixtureDef sensorDef;
    sensorDef.shape = &shape;
    sensorDef.isSensor = true;

    int16_t &i = userData.data.index;
    int16_t numCheckPoints = m_checkPoints.size();
    for (i = 0; i < numCheckPoints; i++) {
        const Rectangle &cp = m_checkPoints[i];

        sensorDef.userData.pointer = userData.pointer;
        int hx = (cp.x1 - cp.x0 + 1) / 2;
        int hy = (cp.y1 - cp.y0 + 1) / 2;
        shape.SetAsBox(p2m(hx), p2m(hy),
                       { p2m(cp.x0 + hx), p2m(cp.y0 + hy) },
                       0.0);
        m_groundBody->CreateFixture(&sensorDef);
    }
}

void TrackSS::populateWorldComputerPoints(b2World &world) const
{
    UserData userData(BodyType::ComputerPoint, 0);

    b2PolygonShape shape;
    b2FixtureDef sensorDef;
    sensorDef.shape = &shape;
    sensorDef.isSensor = true;
    sensorDef.filter.categoryBits = uint16_t(CollisionCategory::CpuRacer);

    int16_t &i = userData.data.index;
    int16_t numComputerPoints = m_computerPoints.size();
    for (i = 0; i < numComputerPoints; i++) {
        const Rectangle &cp = m_computerPoints[i].area;

        sensorDef.userData.pointer = userData.pointer;
        int hx = (cp.x1 - cp.x0 + 1) / 2;
        int hy = (cp.y1 - cp.y0 + 1) / 2;
        shape.SetAsBox(p2m(hx), p2m(hy),
                       { p2m(cp.x0 + hx), p2m(cp.y0 + hy) },
                       0.0);
        m_groundBody->CreateFixture(&sensorDef);
    }
}

void TrackSS::populateWorld(b2World &world)
{
    // Add screen borders
    const float trackWidth = 640;
    const float trackHeight = 368;

    b2PolygonShape hBorder;
    hBorder.SetAsBox(p2m(trackWidth), p2m(0.1));

    b2PolygonShape vBorder;
    vBorder.SetAsBox(p2m(0.1), p2m(trackHeight));

    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position.Set(p2m(trackWidth / 2), p2m(0));
    b2Body *body = world.CreateBody(&bodyDef);
    body->CreateFixture(&hBorder, 1.0);

    bodyDef.position.Set(p2m(trackWidth / 2), p2m(trackHeight));
    body = world.CreateBody(&bodyDef);
    body->CreateFixture(&hBorder, 1.0);

    bodyDef.position.Set(p2m(0), p2m(trackHeight / 2));
    body = world.CreateBody(&bodyDef);
    body->CreateFixture(&vBorder, 1.0);

    bodyDef.position.Set(p2m(trackWidth), p2m(trackHeight / 2));
    body = world.CreateBody(&bodyDef);
    body->CreateFixture(&vBorder, 1.0);

    bodyDef = b2BodyDef();
    m_groundBody = world.CreateBody(&bodyDef);

    int index = 0;
    for (const Object &o: m_objects) {
        const SSTile &tileData = ss_tiles[o.type];
        if (tileData.flags & TileFlag::PhysicsMask) {
            addFixture(m_groundBody, o);
        }

        index++;
    }

    populateWorldCheckPoints(world);
    populateWorldComputerPoints(world);
}

void TrackSS::blit(SDL_Renderer *dest, int dest_x, int dest_y,
                   SDL_Rect *sourceRect)
{
    if (!m_trackBackground) {
        renderBackground(dest);
    }

    SDL_Rect destRect {
        dest_x, dest_y,
    };
    SDL_QueryTexture(m_trackBackground, nullptr, nullptr,
                     &destRect.w, &destRect.h);
    SDL_RenderCopy(dest, m_trackBackground, sourceRect, &destRect);
#ifdef DEBUG_CHECKPOINTS
    blitCheckPoints(dest, dest_x, dest_y);
#endif
#ifdef DEBUG_AI
    blitComputerPoints(dest, dest_x, dest_y);
#endif
}

void TrackSS::blitUpper(SDL_Renderer *dest, int dest_x, int dest_y,
                        SDL_Rect *sourceRect)
{
    for (const CachedTile &tile: m_upperTiles) {
        drawTexture(dest, tile.surface, dest_x + tile.x, dest_y + tile.y,
                    tile.rotation);
    }
}

void TrackSS::placeRacer(int position, RacerPlacement *placement)
{
    int row = position / 2;
    int side = position % 2 == 0 ? -1 : 1;

    const float maxCarLength = 16;
    const float maxCarWidth = 12;
    float distance = 20 - (maxCarLength + 4) * row;

    float frontX = m_startPoint.x + distance * cos(m_startPoint.angle);
    float frontY = m_startPoint.y + distance * sin(m_startPoint.angle);

    float sideDistance = (maxCarWidth + 4) * side;
    // angle is m_startPoint.angle - 90
    float sideAngle = m_startPoint.angle - M_PI_2;
    placement->x = frontX + sideDistance * cos(sideAngle);
    placement->y = frontY + sideDistance * sin(sideAngle);
    placement->angle = m_startPoint.angle - M_PI_2;
}

bool TrackSS::getInfo(int objectIndex, ObjectInfo *objectInfo) const
{
    if (objectIndex >= int(m_objects.size())) return false;
    const Object &o = m_objects[objectIndex];
    const SSTile &tileData = ss_tiles[o.type];

    if (tileData.flags & TileFlag::Grass) {
        objectInfo->flags = ObjectInfo::AffectsDamping;
        objectInfo->linearDamping = 2.0;
        objectInfo->angularDamping = 2.0;
    }
    return true;
}

int TrackSS::distance2ToCheckPoint(int16_t x, int16_t y,
                                   int checkPointIndex) const
{
    const Rectangle &cp = m_checkPoints[checkPointIndex];
    int dx = (cp.x1 + cp.x0) / 2 - x;
    int dy = (cp.y1 + cp.y0) / 2 - y;
    return dx * dx + dy * dy;
}

std::vector<Point> TrackSS::computerPoints() const
{
    std::vector<Point> ret;
    for (const ComputerPoint &cp: m_computerPoints) {
        ret.emplace_back(cp.target);
    }
    return ret;
}

void TrackSS::blitCheckPoints(SDL_Renderer *dest, int dest_x, int dest_y)
{
    int i = 1;
    SDL_SetRenderDrawColor(dest, 255, 255, 0, 255);
    for (const Rectangle &cp: m_checkPoints) {
        SDL_Rect rect {
            cp.x0 + dest_x,
            cp.y0 + dest_y,
            cp.x1 + dest_x,
            cp.y1 + dest_y,
        };
        SDL_RenderDrawRect(dest, &rect);
        drawDebugString(dest,
                        cp.x0 + 4 + dest_x,
                        cp.y0 + 6 + dest_y,
                        "%d", i);
        i++;
    }
}

void TrackSS::blitComputerPoints(SDL_Renderer *dest, int dest_x, int dest_y)
{
    int i = 1;
    SDL_SetRenderDrawColor(dest, 255, 0, 0, 255);
    for (const ComputerPoint &cp: m_computerPoints) {
        SDL_Rect rect {
            cp.area.x0 + dest_x,
            cp.area.y0 + dest_y,
            cp.area.x1 + dest_x,
            cp.area.y1 + dest_y,
        };
        SDL_RenderDrawRect(dest, &rect);
        drawDebugString(dest,
                        cp.target.x + 2 + dest_x,
                        cp.target.y + 2 + dest_y,
                        "%d", i);
        i++;
    }
}

SDL_Surface *TrackSS::createTileSurface(const EmbeddedImage &tile)
{
    return SDL_CreateRGBSurfaceWithFormatFrom(const_cast<uint8_t*>(tile.pixels),
                                 tile.width, tile.height,
                                 tile.bytes_per_pixel * 8,
                                 tile.bytes_per_pixel * tile.width,
                                 SDL_PIXELFORMAT_RGBA32);
}

SDL_Texture *TrackSS::createTileTexture(SDL_Renderer *renderer,
                                        const EmbeddedImage &tile)
{
    SDL_Surface *surface = createTileSurface(tile);
    SDL_Texture *tex = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    return tex;
}

SDL_Surface *TrackSS::rotatedTileSurface(const EmbeddedImage &tile,
                                         int rotation)
{
    SDL_Surface *orig = createTileSurface(tile);
    if (rotation == 0) return orig;

    int dstWidth, dstHeight;
    if (rotation == 2) {
        dstWidth = orig->w;
        dstHeight = orig->h;
    } else {
        dstWidth = orig->h;
        dstHeight = orig->w;
    }
    SDL_Surface *rotated =
        SDL_CreateRGBSurfaceWithFormat(0, dstWidth, dstHeight,
                                       orig->format->BitsPerPixel,
                                       SDL_PIXELFORMAT_RGBA32);
    SDL_LockSurface(orig);
    SDL_LockSurface(rotated);

    uint32_t *pixelSrc = static_cast<uint32_t*>(orig->pixels);
    uint32_t *pixelDst = static_cast<uint32_t*>(rotated->pixels);
    for (int y = 0; y < orig->h; y++)
        for (int x = 0; x < orig->w; x++) {
            int dx, dy;
            if (rotation == 1) {
                dx = orig->h - 1 - y;
                dy = x;
            } else if (rotation == 2) {
                dx = orig->w - 1 - x;
                dy = orig->h - 1 - y;
            } else if (rotation == 3) {
                dx = y;
                dy = orig->w - 1 - x;
            }
            pixelDst[dy * rotated->pitch / 4 + dx] =
                pixelSrc[y * orig->pitch / 4 + x];
        }
    SDL_UnlockSurface(rotated);
    SDL_UnlockSurface(orig);
    SDL_FreeSurface(orig);
    return rotated;
}

void TrackSS::copyAlphaToOverlappingTiles(SDL_Surface *source,
                                  const std::vector<TileSurface> &upperTiles,
                                  int16_t sourceX, int16_t sourceY)
{
    const SDL_PixelFormat *srcFormat = source->format;
    uint8_t srcBpp = srcFormat->BytesPerPixel;
    SDL_LockSurface(source);

    for (const TileSurface &cachedTile: upperTiles) {
        SDL_Surface *dest = cachedTile.surface;

        int startX = std::max(sourceX, cachedTile.x);
        int endX = std::min(sourceX + source->w, cachedTile.x + dest->w);
        int startY = std::max(sourceY, cachedTile.y);
        int endY = std::min(sourceY + source->h, cachedTile.y + dest->h);

        int width = endX - startX;
        int height = endY - startY;
        if (width <= 0 || height <= 0) continue;

        const SDL_PixelFormat *dstFormat = dest->format;
        uint8_t dstBpp = dstFormat->BytesPerPixel;

        SDL_LockSurface(dest);

        int srcX = startX - sourceX;
        int srcY = startY - sourceY;
        int dstX = startX - cachedTile.x;
        int dstY = startY - cachedTile.y;

        uint8_t *srcData = (uint8_t *)source->pixels + srcY * source->pitch + srcX * srcBpp;
        uint8_t *dstData = (uint8_t *)dest->pixels + dstY * dest->pitch + dstX * dstBpp;
        int srcSkip = source->pitch - width * srcBpp;
        int dstSkip = dest->pitch - width * dstBpp;

        fprintf(stderr, "intersection %dx%d, src start %dx%d (size %dx%d) skip %d (p %d, bpp %d), dst start %dx%d (size %dx%d) skip %d (p %d, bpp %d)\n",
                width, height,
                srcX, srcY, source->w, source->h, srcSkip, source->pitch, srcBpp,
                dstX, dstY, dest->w, dest->h, dstSkip, dest->pitch, dstBpp);

        while (height--) {
            GFX_DUFFS_LOOP4({
                uint32_t pixel;
                unsigned sR;
                unsigned sG;
                unsigned sB;
                unsigned sA;
                unsigned dR;
                unsigned dG;
                unsigned dB;
                unsigned dA;
                GFX_DISASSEMBLE_RGBA(srcData, srcBpp, srcFormat, pixel, sR, sG, sB, sA);
                GFX_DISASSEMBLE_RGBA(dstData, dstBpp, dstFormat, pixel, dR, dG, dB, dA);
                dA = std::min(sA, dA);
                // suppress warnings on unused variables
                (void)sR;
                (void)sG;
                (void)sB;
                dstData[0] = 0;
                GFX_ASSEMBLE_RGBA(dstData, dstBpp, dstFormat, dR, dG, dB, dA);
                srcData += srcBpp; dstData += dstBpp;
            }, width);
            srcData += srcSkip;
            dstData += dstSkip;
        }

        SDL_UnlockSurface(dest);
    }
    SDL_UnlockSurface(source);
}

void TrackSS::renderBackground(SDL_Renderer *renderer)
{
    m_trackBackground = SDL_CreateTexture(renderer,
                                          SDL_PIXELFORMAT_RGB565,
                                          SDL_TEXTUREACCESS_TARGET,
                                          640, 368);
    SDL_SetRenderTarget(renderer, m_trackBackground);
    SDL_SetRenderDrawColor(renderer, 0x96, 0x96, 0x96, 255);
    SDL_RenderClear(renderer);

    std::vector<TileSurface> holes;
    std::vector<TileSurface> upperTiles;

    for (const Object &object: m_objects) {
        if (object.type >= ss_tiles_count) continue;

        const SSTile &tileData = ss_tiles[object.type];
        if (tileData.flags & TileFlag::PitStop) {
            // We don't render these
            continue;
        }

        const EmbeddedImage *tile = tileData.image;
        if (tileData.flags & TileFlag::UpperOnly) {
            upperTiles.emplace_back(TileSurface {
                int16_t(object.x * 2),
                int16_t(object.y * 2),
                rotatedTileSurface(*tile, object.rotation),
            });
            if (tileData.flags & TileFlag::Hole) {
                const EmbeddedImage *hole = SSTiles::holeTile(object.type);
                holes.emplace_back(TileSurface {
                    int16_t(object.x * 2),
                    int16_t(object.y * 2),
                    rotatedTileSurface(*hole, object.rotation),
                });
            }
        } else {
            SDL_Texture *tex = createTileTexture(renderer, *tile);
            drawTexture(renderer, tex, object.x * 2, object.y * 2,
                        object.rotation);
            SDL_DestroyTexture(tex);
        }

        if (tileData.flags & TileFlag::HasUpper) {
            const EmbeddedImage *tile = SSTiles::upperTile(object.type);
            SDL_Surface *surface = rotatedTileSurface(*tile, object.rotation);

            upperTiles.emplace_back(TileSurface {
                int16_t(object.x * 2),
                int16_t(object.y * 2),
                surface,
            });
        }
    }

    SDL_SetRenderTarget(renderer, nullptr);

    for (const TileSurface &hole: holes) {
        copyAlphaToOverlappingTiles(hole.surface, upperTiles, hole.x, hole.y);
        SDL_FreeSurface(hole.surface);
    }

    for (const TileSurface &upper: upperTiles) {
        m_upperTiles.emplace_back(CachedTile {
            upper.x, upper.y, 0,
            SDL_CreateTextureFromSurface(renderer, upper.surface),
        });
        SDL_FreeSurface(upper.surface);
    }
}

uint16_t TrackLoaderSS::readWord()
{
    uint16_t xBE = *(uint16_t*)m_ptr;
    m_ptr += 2;
    return SDL_SwapBE16(xBE);
}

uint8_t TrackLoaderSS::readByte()
{
    return *m_ptr++;
}

void TrackLoaderSS::readTile()
{
    /* Two bytes for the X coordinate, 1 for the Y. */
    uint16_t x = readWord();
    uint8_t y = readByte();
    uint8_t type = readByte();
    int8_t rotation = *m_ptr++; // 90 degrees clockwise
    m_track->m_objects.emplace_back(Object{x, y, type, rotation});
}

void TrackLoaderSS::readTiles()
{
    uint16_t ntiles = readWord();

    for (int i = 0; i < ntiles; i++) {
        readTile();
    }
}

void TrackLoaderSS::readCheckPoint()
{
    /* Two bytes for the X coordinate, 1 for the Y. */
    uint16_t x0 = readWord() * 2;
    uint16_t y0 = readByte() * 2;
    uint16_t x1 = readWord() * 2;
    uint16_t y1 = readByte() * 2;
    m_track->m_checkPoints.emplace_back(Rectangle{x0, y0, x1, y1});
}

void TrackLoaderSS::readCheckPoints()
{
    uint16_t npoints = readWord();

    for (int i = 0; i < npoints; i++) {
        readCheckPoint();
    }
}

void TrackLoaderSS::readComputerPoint()
{
    /* Two bytes for the X coordinate, 1 for the Y. */
    uint16_t x0 = readWord() * 2;
    uint16_t y0 = readByte() * 2;
    uint16_t x1 = readWord() * 2;
    uint16_t y1 = readByte() * 2;
    uint16_t px = readWord() * 2;
    uint16_t py = readByte() * 2;
    uint8_t unknown = readByte();
    fprintf(stderr, "Read CPU point at %d,%d - %d,%d target %d,%d (unknown: %d)\n", x0, y0, x1, y1, px, py, unknown);
    m_track->m_computerPoints.emplace_back(ComputerPoint{
        {x0, y0, x1, y1},
        {int16_t(px), int16_t(py)},
    });
}

void TrackLoaderSS::readComputerPoints()
{
    uint16_t npoints = readWord();

    for (int i = 0; i < npoints; i++) {
        readComputerPoint();
    }
}

void TrackLoaderSS::readPanicPoint()
{
    /* Two bytes for the X coordinate, 1 for the Y. */
    uint16_t x = readWord() * 2;
    uint16_t y = readByte() * 2;
    uint8_t unknown = readByte();
    fprintf(stderr, "Read Panic point at %d,%d (unknown: %d)\n", x, y, unknown);
    // unused
}

void TrackLoaderSS::readPanicPoints()
{
    uint16_t npoints = readWord();

    for (int i = 0; i < npoints; i++) {
        readPanicPoint();
    }
}

void TrackLoaderSS::readStartPoint()
{
    /* Two bytes for the X coordinate, 1 for the Y. */
    int16_t x = readWord() * 2;
    int16_t y = readWord() * 2;
    uint8_t rotation = readByte();
    fprintf(stderr, "Read start at %d,%d, rotation %d\n", x, y, rotation);
    m_track->m_startPoint = {
        x, y, rotation * static_cast<float>(M_PI) * 2 / 160 };
}

void TrackLoaderSS::readTrack()
{
    m_ptr = m_data + 4;
    m_version = readWord();
    uint16_t tileOffset = readWord();
    m_ptr = m_data + tileOffset;
    readTiles();
    readCheckPoints();
    readComputerPoints();
    readPanicPoints();
    readStartPoint();
}

bool TrackLoaderSS::checkFormat(const char *data, size_t size) const
{
    return size > 4 &&
        data[0] == 0 &&
        data[1] == 1 &&
        data[2] == 'S' &&
        data[3] == 'S';
}

Track *TrackLoaderSS::load(const char *data, size_t size)
{
    m_data = data;
    m_size = size;
    m_track = new TrackSS;
    readTrack();

    return m_track;
}
