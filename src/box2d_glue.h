#ifndef WS_BOX2D_GLUE_H
#define WS_BOX2D_GLUE_H

#include "b2_fixture.h"

enum class BodyType: int8_t {
    Unknown = 0,
    Racer,
    TrackElement,
    Wall,
    CheckPoint,
    ComputerPoint,
    FinishLine,
    RampFloor,
    RampUpper,
    Hole,
};

enum class CollisionCategory: uint16_t {
    Floor = 0x1,
    Ramp = 0x2,
    Upper = 0x4,
    UnderBridgeWall = 0x8,
    CpuRacer = 0x10,
};

inline CollisionCategory operator |(CollisionCategory a, CollisionCategory b) {
	return static_cast<CollisionCategory>(static_cast<uint16_t>(a) |
                                          static_cast<uint16_t>(b));
}

struct BodyData {
    BodyType bodyType;
    int16_t index;
};

union UserData {
    struct BodyData data;
    uintptr_t pointer;

    UserData(const BodyData &bd): data(bd) {}
    UserData(BodyType type, int index): data({type, int16_t(index)}) {}
    UserData(b2BodyUserData ud): pointer(ud.pointer) {}
    UserData(b2FixtureUserData ud): pointer(ud.pointer) {}
    UserData(const b2Fixture *fixture) {
        b2FixtureUserData ud = fixture->GetUserData();
        if (ud.pointer) {
            *this = UserData(ud);
        } else {
            *this = UserData(fixture->GetBody()->GetUserData());
        }
    }
};

static_assert(sizeof(UserData) == sizeof(uintptr_t));

#endif // WS_BOX2D_GLUE_H
