#ifndef WS_TRACK_H
#define WS_TRACK_H

#include "geometry.h"

#include <SDL.h>
#include <vector>

class b2World;

struct RacerPlacement {
    int16_t x;
    int16_t y;
    float angle;
};

class Track {
public:
    struct ObjectInfo {
        enum Flags {
            None = 0,
            AffectsDamping = 1 << 0,
        };
        Flags flags;
        float linearDamping;
        float angularDamping;
    };

    virtual ~Track() = default;
    virtual void populateWorld(b2World &world) = 0;
    virtual void blit(SDL_Renderer *dest, int dest_x, int dest_y,
                      SDL_Rect *sourceRect) = 0;
    virtual void blitUpper(SDL_Renderer *dest, int dest_x, int dest_y,
                           SDL_Rect *sourceRect) = 0;
    virtual void placeRacer(int position, RacerPlacement *placement) = 0;
    virtual bool getInfo(int objectIndex, ObjectInfo *objectInfo) const = 0;
    virtual int checkPointCount() const = 0;
    virtual int distance2ToCheckPoint(int16_t x, int16_t y,
                                      int checkPointIndex) const = 0;
    virtual std::vector<Point> computerPoints() const = 0;
};

#endif
