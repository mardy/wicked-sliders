#ifndef WS_SS_TILES_H
#define WS_SS_TILES_H

#include "bitmask_operators.h"
#include "embedded_image.h"

enum TileFlag {
    None = 0,
    Finish = 1 << 0,
    Wall = 1 << 1,
    Grass = 1 << 2,
    // Ramp is always W to E or SW to NE
    Ramp = 1 << 3,
    Hole = 1 << 4,
    PhysicsMask = 0xffff,
    Rectangular = 1 << 16,
    Circular = 1 << 17,
    FloorOnly = 1 << 18,
    UpperOnly = 1 << 19,
    HasUpper = 1 << 20,
    PitStop = 1 << 21,
};

template<>
struct enable_bitmask_operators<TileFlag>{
    static constexpr bool enable=true;
};

struct SSTile {
    const EmbeddedImage *image;
    TileFlag flags;
};

extern const SSTile ss_tiles[];
extern int ss_tiles_count;

struct SSTileShape {
    TileFlag flags;
    bool isClosed;
    int8_t numPoints;
    const int8_t *points;
};

struct SSTileShapes {
    uint8_t tileIndex;
    int8_t numShapes;
    const SSTileShape *shapes;
};

extern const SSTileShapes ss_tile_shapes[];
extern int ss_tile_shapes_count;

namespace SSTiles {

const SSTileShapes *shapesForTile(uint8_t tileIndex);

/* Call this for tiles having the HasUpper flag */
const EmbeddedImage *upperTile(uint8_t tileIndex);

/* Call this for tiles having the Hole flag */
const EmbeddedImage *holeTile(uint8_t tileIndex);

} // namespace

#endif // WS_SS_TILES_H
