#ifndef WS_FONTS_H
#define WS_FONTS_H

#include <cstddef>

struct FontFile {
    const char *data;
    size_t size;
};

namespace Fonts {

enum class Type {
    Title,
    Text,
    LastFont,
};

const FontFile &fontFile(Type type);

} // namespace

#endif // WS_FONTS_H
