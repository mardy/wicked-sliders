#ifndef WS_CAR_DATA_H
#define WS_CAR_DATA_H

#include "car_parameters.h"

#include <cstdint>

enum class CarLevel {
    Floor = 0,
    RampUpwards,
    RampDownwards,
    Upper,
};

struct CarData {
    float x, y;
    CarLevel level;
    // physical angle of the car (0 is downwards, + is clockwise)
    float angle;

    // effective speed
    float speed_angle;
    float speed;

    // Set by the racer:
    float steer;  // negative for left, positive for right
    float acceleration;

    // Fixed parameters
    CarParameters params;
};

#endif // WS_CAR_DATA_H
