#ifndef WS_RACE_PANEL_H
#define WS_RACE_PANEL_H

#include "font_renderer.h"
#include "point_animator.h"

#include <SDL.h>
#include <cstdint>

struct GameData;

class RacePanel {
public:
    RacePanel();
    virtual ~RacePanel();

    FontRenderer &fontRenderer() { return m_playerFont; }

    void update(const GameData &gameData, int elapsedMs);

    void blit(SDL_Renderer *renderer, int off_x, int off_y,
              const GameData &gameData);

private:
    int16_t m_playerBoxHeight;
    int16_t m_playerBoxWidth;
    int m_playerBoxesPerColumn;
    PointAnimator m_playerBoxesAnimator;
    FontRenderer m_playerFont;
};

#endif // WS_RACE_PANEL_H
