#ifndef WS_KEYBOARD_INPUT_H
#define WS_KEYBOARD_INPUT_H

#include "input_device.h"

#include <SDL.h>
#include <climits>

class KeyboardInput: public InputDevice {
public:
    KeyboardInput(const Uint8 *state): m_state(state) {
        m_buttons = 2;
        m_buttonMapping[0] = SDL_SCANCODE_RETURN;
        m_buttonMapping[1] = SDL_SCANCODE_BACKSPACE;
        m_axes = 0;
        m_hats = 0;
    }

    bool update() override {
        m_x = m_y = 0;
        if (m_state[SDL_SCANCODE_LEFT]) m_x -= SHRT_MAX;
        if (m_state[SDL_SCANCODE_RIGHT]) m_x += SHRT_MAX;
        if (m_state[SDL_SCANCODE_UP]) m_y -= SHRT_MAX;
        if (m_state[SDL_SCANCODE_DOWN]) m_y += SHRT_MAX;
        return true;
    }

    bool buttonIsPressed(int index) override {
        return m_state[m_buttonMapping[index]];
    }

    std::string name() const override { return "Keyboard"; }

private:
    const Uint8 *m_state;
    int m_buttonMapping[2]; // We only use two buttons in this game
};

#endif // WS_KEYBOARD_INPUT_H
