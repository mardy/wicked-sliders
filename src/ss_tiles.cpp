#include "ss_tiles.h"

#include "ss_tile_0.h"
#include "ss_tile_1.h"
#include "ss_tile_2.h"
#include "ss_tile_3.h"
#include "ss_tile_4.h"
#include "ss_tile_5.h"
#include "ss_tile_6.h"
#include "ss_tile_7.h"
#include "ss_tile_8.h"
#include "ss_tile_9.h"

#include "ss_tile_10.h"
#include "ss_tile_11.h"
#include "ss_tile_12.h"
#include "ss_tile_13.h"
#include "ss_tile_14.h"
#include "ss_tile_15.h"
#include "ss_tile_16.h"
#include "ss_tile_17.h"
#include "ss_tile_18.h"
#include "ss_tile_19.h"

#include "ss_tile_20.h"
#include "ss_tile_21.h"
#include "ss_tile_22.h"
#include "ss_tile_23.h"
#include "ss_tile_24.h"
#include "ss_tile_25.h"
#include "ss_tile_26.h"
#include "ss_tile_27.h"
#include "ss_tile_28.h"
#include "ss_tile_29.h"

#include "ss_tile_30.h"
#include "ss_tile_31.h"
#include "ss_tile_32.h"
#include "ss_tile_33.h"
#include "ss_tile_34.h"
#include "ss_tile_35.h"
#include "ss_tile_36.h"
#include "ss_tile_37.h"
#include "ss_tile_38.h"
#include "ss_tile_39_0.h"
#include "ss_tile_39_1.h"

#include "ss_tile_40_0.h"
#include "ss_tile_40_1.h"
#include "ss_tile_41.h"
#include "ss_tile_42.h"
#include "ss_tile_43.h"
#include "ss_tile_44.h"
#include "ss_tile_45.h"
#include "ss_tile_46.h"
#include "ss_tile_47.h"
#include "ss_tile_48.h"
#include "ss_tile_49.h"

#include "ss_tile_50.h"
#include "ss_tile_51_0.h"
#include "ss_tile_51_1.h"
#include "ss_tile_52_0.h"
#include "ss_tile_52_1.h"
#include "ss_tile_53.h"
#include "ss_tile_53_1.h"
#include "ss_tile_54.h"
#include "ss_tile_55.h"
#include "ss_tile_56.h"
#include "ss_tile_57.h"
#include "ss_tile_58.h"
#include "ss_tile_59.h"

#include "ss_tile_60.h"
#include "ss_tile_61.h"
#include "ss_tile_62.h"
#include "ss_tile_63.h"
#include "ss_tile_64.h"
#include "ss_tile_65.h"
#include "ss_tile_66.h"
#include "ss_tile_67.h"
#include "ss_tile_68.h"
#include "ss_tile_69.h"

#include "ss_tile_70.h"
#include "ss_tile_71.h"
#include "ss_tile_72.h"
#include "ss_tile_73.h"
#include "ss_tile_74.h"
#include "ss_tile_75.h"
#include "ss_tile_76.h"
#include "ss_tile_77.h"
#include "ss_tile_78.h"
#include "ss_tile_79.h"

#include "ss_tile_80.h"
#include "ss_tile_81.h"
#include "ss_tile_82.h"
#include "ss_tile_83.h"
#include "ss_tile_84.h"
#include "ss_tile_85.h"
#include "ss_tile_86.h"
#include "ss_tile_87.h"
#include "ss_tile_88.h"
#include "ss_tile_89.h"

#include "ss_tile_90.h"
#include "ss_tile_91.h"
#include "ss_tile_92.h"
#include "ss_tile_93.h"
#include "ss_tile_94.h"
#include "ss_tile_95.h"
#include "ss_tile_96.h"
#include "ss_tile_97.h"
#include "ss_tile_98.h"
#include "ss_tile_99.h"

#include "ss_tile_100.h"
#include "ss_tile_101.h"
#include "ss_tile_102.h"
#include "ss_tile_103.h"
#include "ss_tile_104.h"
#include "ss_tile_105.h"
#include "ss_tile_106.h"
#include "ss_tile_107.h"
#include "ss_tile_108.h"
#include "ss_tile_109.h"


#define TILE(ptr) reinterpret_cast<const EmbeddedImage*>(ptr)

const SSTile ss_tiles[] = {
    { TILE(&img_ss_tile_0), TileFlag::Finish | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_1), TileFlag::Finish, },
    { TILE(&img_ss_tile_2), TileFlag::None, },
    { TILE(&img_ss_tile_3), TileFlag::None, },
    { TILE(&img_ss_tile_4), TileFlag::None, },
    { TILE(&img_ss_tile_5), TileFlag::None, },
    { TILE(&img_ss_tile_6), TileFlag::None, },
    { TILE(&img_ss_tile_7), TileFlag::None, },
    { TILE(&img_ss_tile_8), TileFlag::None, },
    { TILE(&img_ss_tile_9), TileFlag::Rectangular, },

    { TILE(&img_ss_tile_10), TileFlag::None, },
    { TILE(&img_ss_tile_11), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_12), TileFlag::None, },
    { TILE(&img_ss_tile_13), TileFlag::None, },
    { TILE(&img_ss_tile_14), TileFlag::None, },
    { TILE(&img_ss_tile_15), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_16), TileFlag::None, },
    { TILE(&img_ss_tile_17), TileFlag::Wall, },
    { TILE(&img_ss_tile_18), TileFlag::Wall | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_19), TileFlag::Wall, },

    { TILE(&img_ss_tile_20), TileFlag::Wall | TileFlag::Circular, },
    { TILE(&img_ss_tile_21), TileFlag::Wall | TileFlag::Circular, },
    { TILE(&img_ss_tile_22), TileFlag::Wall, },
    { TILE(&img_ss_tile_23), TileFlag::Wall | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_24), TileFlag::Grass | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_25), TileFlag::Grass | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_26), TileFlag::Grass | TileFlag::Circular, },
    { TILE(&img_ss_tile_27), TileFlag::Grass | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_28), TileFlag::None, },
    { TILE(&img_ss_tile_29), TileFlag::None, },

    { TILE(&img_ss_tile_30), TileFlag::None, },
    { TILE(&img_ss_tile_31), TileFlag::Wall | TileFlag::HasUpper, },
    { TILE(&img_ss_tile_32), TileFlag::Wall | TileFlag::HasUpper, },
    { TILE(&img_ss_tile_33), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_34), TileFlag::None, },
    { TILE(&img_ss_tile_35), TileFlag::None, },
    { TILE(&img_ss_tile_36), TileFlag::None, },
    { TILE(&img_ss_tile_37), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_38), TileFlag::None, },
    { TILE(&img_ss_tile_39_0), TileFlag::HasUpper | TileFlag::Ramp, },

    { TILE(&img_ss_tile_40_0), TileFlag::HasUpper | TileFlag::Ramp, },
    { TILE(&img_ss_tile_41), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_42), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_43), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_44), TileFlag::Grass | TileFlag::Circular, },
    { TILE(&img_ss_tile_45), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_46), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_47), TileFlag::None, },
    { TILE(&img_ss_tile_48), TileFlag::None, },
    { TILE(&img_ss_tile_49), TileFlag::Rectangular, },

    { TILE(&img_ss_tile_50), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_51_0), TileFlag::HasUpper | TileFlag::Ramp, },
    { TILE(&img_ss_tile_52_0), TileFlag::HasUpper | TileFlag::Ramp, },
    { TILE(&img_ss_tile_53), TileFlag::Hole | TileFlag::UpperOnly, },
    { TILE(&img_ss_tile_54), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_55), TileFlag::None, },
    { TILE(&img_ss_tile_56), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_57), TileFlag::None, },
    { TILE(&img_ss_tile_58), TileFlag::None, },
    { TILE(&img_ss_tile_59), TileFlag::UpperOnly, },

    { TILE(&img_ss_tile_60), TileFlag::UpperOnly | TileFlag::Wall, },
    { TILE(&img_ss_tile_61), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_62), TileFlag::None, },
    { TILE(&img_ss_tile_63), TileFlag::None, },
    { TILE(&img_ss_tile_64), TileFlag::None, },
    { TILE(&img_ss_tile_65), TileFlag::UpperOnly, },
    { TILE(&img_ss_tile_66), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_67), TileFlag::None, },
    { TILE(&img_ss_tile_68), TileFlag::Rectangular | TileFlag::PitStop, },
    { TILE(&img_ss_tile_69), TileFlag::None | TileFlag::PitStop, },

    { TILE(&img_ss_tile_70), TileFlag::None, },
    { TILE(&img_ss_tile_71), TileFlag::Wall | TileFlag::UpperOnly, },
    { TILE(&img_ss_tile_72), TileFlag::None, },
    { TILE(&img_ss_tile_73), TileFlag::None, },
    { TILE(&img_ss_tile_74), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_75), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_76), TileFlag::None, },
    { TILE(&img_ss_tile_77), TileFlag::None, },
    { TILE(&img_ss_tile_78), TileFlag::None, },
    { TILE(&img_ss_tile_79), TileFlag::None, },

    { TILE(&img_ss_tile_80), TileFlag::None, },
    { TILE(&img_ss_tile_81), TileFlag::None, },
    { TILE(&img_ss_tile_82), TileFlag::None, },
    { TILE(&img_ss_tile_83), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_84), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_85), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_86), TileFlag::None, },
    { TILE(&img_ss_tile_87), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_88), TileFlag::None, },
    { TILE(&img_ss_tile_89), TileFlag::None, },

    { TILE(&img_ss_tile_90), TileFlag::Rectangular |
                             TileFlag::UpperOnly | TileFlag::Wall, },
    { TILE(&img_ss_tile_91), TileFlag::UpperOnly, },
    { TILE(&img_ss_tile_92), TileFlag::None, },
    { TILE(&img_ss_tile_93), TileFlag::Wall | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_94), TileFlag::Wall | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_95), TileFlag::None, },
    { TILE(&img_ss_tile_96), TileFlag::Wall | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_97), TileFlag::Wall | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_98), TileFlag::Wall | TileFlag::Rectangular, },
    { TILE(&img_ss_tile_99), TileFlag::Wall | TileFlag::Rectangular, },

    { TILE(&img_ss_tile_100), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_101), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_102), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_103), TileFlag::None, },
    { TILE(&img_ss_tile_104), TileFlag::None, },
    { TILE(&img_ss_tile_105), TileFlag::Rectangular, },
    { TILE(&img_ss_tile_106), TileFlag::None, },
    { TILE(&img_ss_tile_107), TileFlag::None, },
    { TILE(&img_ss_tile_108), TileFlag::None, },
    { TILE(&img_ss_tile_109), TileFlag::None, },
};

int ss_tiles_count = sizeof(ss_tiles) / sizeof(ss_tiles[0]);

#define SHAPES(var) (sizeof(var) / sizeof(var[0])), var
#define POINTS(var) (sizeof(var) / sizeof(var[0])) / 2, var

static const int8_t pts_1[] = { 0, 25, 7, 32, 32, 7, 25, 0 };
static const SSTileShape shapes_1[] = {
    { TileFlag::Finish, true, POINTS(pts_1) },
};

static const int8_t pts_17_1[] = {
    32, 10, 10, 50, 0, 50, 0, 31, 5, 18, 18, 5, 31, 0, 48, 0, 48, 8, 34, 8
};
static const int8_t pts_17_2[] = {
    50, 8, 48, 8, 34, 8, 23, 13, 15, 21, 10, 32, 10, 50, 10, 52,
};
static const SSTileShape shapes_17[] = {
    { TileFlag::Wall, false, POINTS(pts_17_1) },
    { TileFlag::Wall, false, POINTS(pts_17_2) },
};

static const int8_t pts_19[] = { 0, 29, 5, 34, 7, 34, 36, 5, 31, 0, 29, 0 };
static const SSTileShape shapes_19[] = {
    { TileFlag::Wall, true, POINTS(pts_19) },
};

static const int8_t pts_22[] = {
    9, 22, 15, 29, 23, 29, 29, 23, 29, 15, 23, 9, 15, 9, 9, 15 };
static const SSTileShape shapes_22[] = {
    { TileFlag::Wall, true, POINTS(pts_22) },
};

static const int8_t pts_31[] = {
    13, 7, 7, 13, 7, 21, 13, 27, 25, 27, 31, 21, 31, 13, 25, 7 };
static const SSTileShape shapes_31[] = {
    { TileFlag::Wall, true, POINTS(pts_31) },
};

static const int8_t pts_32[] = {
    10, 6, 6, 10, 6, 14, 10, 18, 16, 18, 20, 14, 20, 10, 16, 6};
static const SSTileShape shapes_32[] = {
    { TileFlag::Wall, true, POINTS(pts_32) },
};

static const int8_t pts_39_0[] = { 4, 3, 4, 8, 18, 8, 18, 0, 7, 0 };
static const int8_t pts_39_1[] = { 18, 0, 18, 8, 70, 8, 70, 0 };
static const int8_t pts_39_2[] = { 4, 6, 4, 8, 4, 48, 4, 50 };
static const int8_t pts_39_3[] = { 18, 6, 18, 8, 18, 48, 18, 50 };
static const int8_t pts_39_4[] = { 18, -2, 18, 0, 18, 48, 18, 50 };
static const SSTileShape shapes_39[] = {
    { TileFlag::Wall, true, POINTS(pts_39_0) },
    { TileFlag::Wall|TileFlag::UpperOnly, true, POINTS(pts_39_1) },
    // Ramp enter (floor)
    { TileFlag::Ramp|TileFlag::FloorOnly, false, POINTS(pts_39_2) },
    // Ramp exit (upper)
    { TileFlag::Ramp|TileFlag::UpperOnly, false, POINTS(pts_39_3) },
    { TileFlag::Wall|TileFlag::FloorOnly, false, POINTS(pts_39_4) },
};

static const int8_t pts_40_0[] = { 4, 40, 4, 45, 7, 48, 18, 48, 18, 40 };
static const int8_t pts_40_1[] = { 18, 40, 18, 48, 70, 48, 70, 40 };
static const int8_t pts_40_2[] = { 4, -2, 4, 0, 4, 40, 4, 42 };
static const int8_t pts_40_3[] = { 18, -2, 18, 0, 18, 40, 18, 42 };
static const SSTileShape shapes_40[] = {
    { TileFlag::Wall, true, POINTS(pts_40_0) },
    { TileFlag::Wall|TileFlag::UpperOnly, true, POINTS(pts_40_1) },
    // Ramp enter (floor)
    { TileFlag::Ramp|TileFlag::FloorOnly, false, POINTS(pts_40_2) },
    // Ramp exit (upper)
    { TileFlag::Ramp|TileFlag::UpperOnly, false, POINTS(pts_40_3) },
    { TileFlag::Wall|TileFlag::FloorOnly, false, POINTS(pts_39_4) }, // same as tile 39
};

static const int8_t pts_51_0[] = { 0, 42, 0, 47, 4, 51, 14, 41, 8, 35 };
static const int8_t pts_51_1[] = { 8, 35, 14, 41, 50, 5, 45, 0 };
static const int8_t pts_51_2[] = { 2, 50, 3, 51, 32, 80, 33, 81 };
static const int8_t pts_51_3[] = { 13, 30, 14, 41, 42, 69, 43, 70 };
static const int8_t pts_51_4[] = { 7, 34, 8, 35, 42, 69, 43, 70 };
static const SSTileShape shapes_51[] = {
    { TileFlag::Wall, true, POINTS(pts_51_0) },
    { TileFlag::Wall|TileFlag::UpperOnly, true, POINTS(pts_51_1) },
    // Ramp enter (floor)
    { TileFlag::Ramp|TileFlag::FloorOnly, false, POINTS(pts_51_2) },
    // Ramp exit (upper)
    { TileFlag::Ramp|TileFlag::UpperOnly, false, POINTS(pts_51_3) },
    { TileFlag::Wall|TileFlag::FloorOnly, false, POINTS(pts_51_4) },
};

static const int8_t pts_52_0[] = { 31, 74, 35, 78, 39, 78, 47, 70, 41, 64 };
static const int8_t pts_52_1[] = { 41, 64, 47, 70, 82, 33, 77, 28 };
static const int8_t pts_52_2[] = { 1, 45, 2, 46, 31, 75, 32, 76 };
static const int8_t pts_52_3[] = { 12, 35, 13, 36, 41, 64, 42, 65 };
static const int8_t pts_52_4[] = { 12, 35, 13, 36, 46, 69, 47, 70 };
static const SSTileShape shapes_52[] = {
    { TileFlag::Wall, true, POINTS(pts_52_0) },
    { TileFlag::Wall|TileFlag::UpperOnly, true, POINTS(pts_52_1) },
    // Ramp enter (floor)
    { TileFlag::Ramp|TileFlag::FloorOnly, false, POINTS(pts_52_2) },
    // Ramp exit (upper)
    { TileFlag::Ramp|TileFlag::UpperOnly, false, POINTS(pts_52_3) },
    { TileFlag::Wall|TileFlag::FloorOnly, false, POINTS(pts_52_4) },
};

static const int8_t pts_53_0[] = {
    6, 2, 12, 2, 16, 6, 16, 12, 12, 16, 6, 16, 2, 12, 2, 6 };
static const SSTileShape shapes_53[] = {
    { TileFlag::Hole|TileFlag::UpperOnly, true, POINTS(pts_53_0) },
};

static const int8_t pts_60_0[] = {
    12, -1, 12, 0, 12, 8, 10, 12, 6, 14, 0, 14, -1, 14 };
static const SSTileShape shapes_60[] = {
    { TileFlag::Wall|TileFlag::UpperOnly, false, POINTS(pts_60_0) },
};

static const int8_t pts_71_0[] = {
    -2, 56, 0, 56, 26, 56, 38, 52, 45, 47, 50, 40, 54, 28, 54, 0, 54, -2 };
static const SSTileShape shapes_71[] = {
    { TileFlag::Wall|TileFlag::UpperOnly, false, POINTS(pts_71_0) },
};

const SSTileShapes ss_tile_shapes[] = {
    { 1, SHAPES(shapes_1) },
    { 17, SHAPES(shapes_17) },
    { 19, SHAPES(shapes_19) },
    { 22, SHAPES(shapes_22) },
    { 31, SHAPES(shapes_31) },
    { 32, SHAPES(shapes_32) },
    { 39, SHAPES(shapes_39) },
    { 40, SHAPES(shapes_40) },
    { 51, SHAPES(shapes_51) },
    { 52, SHAPES(shapes_52) },
    { 53, SHAPES(shapes_53) },
    { 60, SHAPES(shapes_60) },
    { 71, SHAPES(shapes_71) },
};

#undef SHAPES
#undef POINTS

int ss_tile_shapes_count = sizeof(ss_tile_shapes) / sizeof(ss_tile_shapes[0]);

const SSTileShapes *SSTiles::shapesForTile(uint8_t tileIndex)
{
    for (int i = 0; i < ss_tile_shapes_count; i++) {
        const SSTileShapes *shapes = &ss_tile_shapes[i];
        if (shapes->tileIndex == tileIndex) {
            return shapes;
        }
    }
    return nullptr;
}

const EmbeddedImage *SSTiles::upperTile(uint8_t tileIndex)
{
    switch (tileIndex) {
    case 31: return TILE(&img_ss_tile_31);
    case 32: return TILE(&img_ss_tile_32);
    case 39: return TILE(&img_ss_tile_39_1);
    case 40: return TILE(&img_ss_tile_40_1);
    case 51: return TILE(&img_ss_tile_51_1);
    case 52: return TILE(&img_ss_tile_52_1);
    }
    return nullptr;
}

const EmbeddedImage *SSTiles::holeTile(uint8_t tileIndex)
{
    switch (tileIndex) {
    case 53: return TILE(&img_ss_tile_53_1);
    }
    return nullptr;
}
