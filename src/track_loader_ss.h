#ifndef WS_TRACK_LOADER_SS_H
#define WS_TRACK_LOADER_SS_H

#include "track_loader.h"

#include <stdint.h>

struct TrackSS;

class TrackLoaderSS: public TrackLoader {
protected:
    bool checkFormat(const char *data, size_t size) const override;
    Track *load(const char *data, size_t size) override;

private:
    inline uint16_t readWord();
    inline uint8_t readByte();
    void readTile();
    void readTiles();
    void readCheckPoint();
    void readCheckPoints();
    void readComputerPoint();
    void readComputerPoints();
    void readPanicPoint();
    void readPanicPoints();
    void readStartPoint();
    void readTrack();

    const char *m_data;
    size_t m_size;
    uint16_t m_version;
    TrackSS *m_track;
    const char *m_ptr;
};

#endif // WS_TRACK_LOADER_SS_H
