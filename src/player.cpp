#include "player.h"

#include "car_model.h"
#include "cpu_racer.h"
#include "human_racer.h"
#include "input_device.h"

class PlayerPrivate {
public:
    PlayerPrivate(InputDevice *input);

private:
    friend class Player;
    InputDevice *m_input;
    std::string m_name;
    RgbColor m_color;
    CarModel m_carModel;
};

PlayerPrivate::PlayerPrivate(InputDevice *input):
    m_input(input)
{
}

Player::Player(InputDevice *input):
    impl(new PlayerPrivate(input))
{
}

Player::Player(Player &&o) = default;
Player::~Player() = default;

void Player::setName(std::string name)
{
    impl->m_name = std::move(name);
}

const std::string &Player::name() const
{
    return impl->m_name;
}

void Player::setColor(RgbColor color)
{
    impl->m_color = color;
}

RgbColor Player::color() const
{
    return impl->m_color;
}

void Player::setCarModel(const CarModel &carModel)
{
    impl->m_carModel = carModel;
}

const CarModel &Player::carModel() const
{
    return impl->m_carModel;
}

InputDevice *Player::inputDevice() const
{
    return impl->m_input;
}

Racer *Player::makeRacer() const
{
    Racer *racer = nullptr;
    if (inputDevice()) {
        racer = new HumanRacer(name(), inputDevice(),
                               carModel(), color());
    } else {
        racer = new CpuRacer(name(), CpuRacer::Level::Novice,
                             carModel(), color());
    }
    return racer;
}
