#ifndef WS_EMBEDDED_IMAGE_H
#define WS_EMBEDDED_IMAGE_H

#include <cstdint>

struct EmbeddedImage {
    int width;
    int height;
    char bytes_per_pixel;
    uint8_t pixels[];
};

#endif // WS_EMBEDDED_IMAGE_H
