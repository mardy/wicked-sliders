#include "cpu_racer.h"

#include "box2d_glue.h"
#include "debug.h"
#include "game_data.h"
#include "units.h"

#include <array>
#include <b2_body.h>
#include <b2_chain_shape.h>
#include <b2_contact.h>
#include <b2_polygon_shape.h>

struct LevelParameters {
    int reactionTime; // in milliseconds
    float maxDeviation;
};

static const std::array<LevelParameters, CpuRacer::NumLevels> s_levels = {
    // Novice
    LevelParameters {
        100,
        0.2,
    },
    // Intermediate
    LevelParameters {
        50,
        0.1,
    },
    // Expert
    LevelParameters {
        0,
        0.01,
    },
};

class CpuRacerPrivate {
public:
    CpuRacerPrivate(CpuRacer::Level level, CpuRacer *q);

    void updateInput(const GameData &gd,
                     const GameData::RacerData &racerData,
                     const LevelParameters &lp);
    void decideBypassDirection(const b2Vec2 &carPos,
                               const b2Fixture *fixture,
                               const b2Vec2 &target);

private:
    friend class CpuRacer;
    float m_steer;
    float m_acceleration;
    b2Vec2 m_lastContact;
    uint32_t m_firstContactTimeMs = 0;
    uint32_t m_lastContactTimeMs = 0;
    uint32_t m_lastUpdateTimeMs = 0;
    uint32_t m_bypassModeStartTimeMs = 0;
    bool m_bypassModeConsidered = false;
    bool m_bypassModeActive = false;
    bool m_bypassDirectionRight; // if false, we will go left
    CpuRacer::Level m_level;
    CpuRacer *q;
};

CpuRacerPrivate::CpuRacerPrivate(CpuRacer::Level level, CpuRacer *q):
    m_level(level),
    q(q)
{
}

static float direction(const b2Vec2 &from, const b2Vec2 &to)
{
    b2Vec2 dir = to - from;
    return atan2f(-dir.y, dir.x);
}

static void findShapeCorners(const b2PolygonShape *shape, const b2Transform &trans,
                             const b2Vec2 &carPos,
                             b2Vec2 *pointLeft, b2Vec2 *pointRight)
{
    float left, right, prev;
    bool first = true;

    for (int i = 0; i < shape->m_count; i++) {
        const b2Vec2 p = b2Mul(trans, shape->m_vertices[i]);
        float angle = direction(carPos, p);
        debug_ai("Point (%.1f,%.1f), angle %.1f", p.x, p.y, angle * 180 / M_PI);
        if (first) {
            left = right = angle;
            *pointLeft = *pointRight = p;
            first = false;
        } else {
            float diff = normalizeAngle(angle - prev);
            if (diff > 0) {
                diff = normalizeAngle(angle - left);
                if (diff > 0) {
                    left = angle;
                    *pointLeft = p;
                }
            } else {
                diff = normalizeAngle(angle - right);
                if (diff < 0) {
                    right = angle;
                    *pointRight = p;
                }
            }
        }
        prev = angle;
    }
}

static void findShapeCorners(const b2ChainShape *shape, const b2Transform &trans,
                             const b2Vec2 &carPos,
                             b2Vec2 *pointLeft, b2Vec2 *pointRight)
{
    // TODO
}

static void findShapeCorners(const b2Shape *shape, const b2Transform &trans,
                             const b2Vec2 &carPos,
                             b2Vec2 *pointLeft, b2Vec2 *pointRight)
{
    switch (shape->GetType()) {
    case b2Shape::e_polygon:
        findShapeCorners(static_cast<const b2PolygonShape*>(shape), trans,
                         carPos, pointLeft, pointRight);
        break;
    case b2Shape::e_chain:
        findShapeCorners(static_cast<const b2ChainShape*>(shape), trans,
                         carPos, pointLeft, pointRight);
        break;
    }
}

void CpuRacerPrivate::decideBypassDirection(const b2Vec2 &carPos,
                                            const b2Fixture *fixture,
                                            const b2Vec2 &target)
{
    const b2Shape *shape = fixture->GetShape();
    b2Vec2 pointLeft, pointRight;
    auto transform = fixture->GetBody()->GetTransform();
    findShapeCorners(shape, transform, carPos, &pointLeft, &pointRight);

    /* pick the direction that minimizes:
     *   dist(carPos, corner) + dist(corner, target)
     */
    float distanceLeft = b2Distance(carPos, pointLeft) +
        b2Distance(pointLeft, target);
    float distanceRight = b2Distance(carPos, pointRight) +
        b2Distance(pointRight, target);
    debug_ai("Car (%.1f,%.1f), target (%.1f,%.1f), "
             "left (%.1f,%.1f), right (%.1f,%.1f) distance: %.1f vs %.1f",
             carPos.x, carPos.y, target.x, target.y,
             pointLeft.x, pointLeft.y, pointRight.x, pointRight.y,
             distanceLeft, distanceRight);

    m_bypassDirectionRight = distanceRight < distanceLeft;
}

void CpuRacerPrivate::updateInput(const GameData &gd,
                                  const GameData::RacerData &racerData,
                                  const LevelParameters &lp)
{
    Point target = gd.computerPoints[racerData.nextComputerPoint];
    CarData &carData = q->carData();

    uint32_t elapsedMs = gd.currentTimeMs - m_lastUpdateTimeMs;
    m_lastUpdateTimeMs = gd.currentTimeMs;

    uint32_t elapsedSinceLastContact = gd.currentTimeMs - m_lastContactTimeMs;
    b2Vec2 carPos = { p2m(carData.x), p2m(carData.y) };

    if (elapsedSinceLastContact > 200 ||
        b2DistanceSquared(m_lastContact, carPos) > 2.0) {
        if (m_bypassModeActive) {
            debug_ai("Player %d left bypassmode", q->playerIndex());
            m_bypassModeActive = false;
        }
        m_bypassModeConsidered = false;
    }

    float dx = target.x - carData.x;
    float dy = target.y - carData.y;
    float targetAngle = fmodf(atan2f(dx, dy), M_PI * 2);

    b2Fixture *contactFixture = nullptr;
    for (b2ContactEdge* ce = q->body()->GetContactList(); ce; ce = ce->next) {
        b2Contact *c = ce->contact;
        b2Fixture *fixture = c->GetFixtureA();
        if (!fixture->IsSensor() && fixture->GetBody()->GetType() != b2_dynamicBody) {
            contactFixture = fixture;
            break;
        }
    }

    if (contactFixture) {
        if (m_bypassModeConsidered) {
            if (gd.currentTimeMs - m_firstContactTimeMs > 100 &&
                b2DistanceSquared(m_lastContact, carPos) < 0.05) {
                m_bypassModeActive = true;
                m_bypassModeConsidered = false;
                b2Vec2 targetPoint { p2m(target.x), p2m(target.y) };
                decideBypassDirection(carPos, contactFixture, targetPoint);
                debug_ai("Player %d entered bypassmode", q->playerIndex());
            }
        } else if (!m_bypassModeActive) {
            m_firstContactTimeMs = gd.currentTimeMs;
            m_bypassModeConsidered = true;
        }

        m_lastContact = carPos;
        m_lastContactTimeMs = gd.currentTimeMs;
    }
    if (m_bypassModeActive) {
        m_steer = m_bypassDirectionRight ? 0.6 : -0.6;
        m_acceleration = 1.0;
        return;
    }

    /* Apply the deviation handicap */
    float deviation = lp.maxDeviation *
        static_cast<float>(rand() - RAND_MAX / 2) / static_cast<float>(RAND_MAX / 2);
    targetAngle += deviation;

    float angle_diff = fmodf(targetAngle - carData.angle, M_PI * 2);
    if (angle_diff < 0.0) {
        angle_diff += M_PI * 2;
    }
    float steer = 0.0;
    if (angle_diff < M_PI) {
        steer = -angle_diff / M_PI;
    } else if (angle_diff > 0) {
        steer = (M_PI * 2 - angle_diff) / M_PI;
    }
    /* This makes the steering less natural, but much more effective. */
    if (steer < 0) steer = -1.0;
    else if (steer > 0) steer = 1.0;
    float acceleration = 1.0;
    acceleration *= cosf(angle_diff);

    m_steer = steer;
    m_acceleration = acceleration;
}

CpuRacer::CpuRacer(const std::string &name,
                   Level level,
                   const CarModel &carModel,
                   RgbColor color):
    Racer(name, carModel, color),
    impl(new CpuRacerPrivate(level, this))
{
    setBaseFilterMask(uint16_t(CollisionCategory::CpuRacer));
}

CpuRacer::~CpuRacer() = default;

void CpuRacer::updateInput(const GameData &gd)
{
    uint32_t elapsed = gd.currentTimeMs - impl->m_lastUpdateTimeMs;
    const LevelParameters &levelParameters = s_levels[impl->m_level];
    const GameData::RacerData &racerData = gd.racers[playerIndex()];

    impl->updateInput(gd, racerData, levelParameters);
    setControls(impl->m_steer, impl->m_acceleration);
}

void CpuRacer::blit(SDL_Renderer *dest, int off_x, int off_y)
{
    Racer::blit(dest, off_x, off_y);
#ifdef DEBUG_AI
    SDL_SetRenderDrawColor(dest, 80, 0, 0, 255);
    if (impl->m_bypassModeConsidered) {
        SDL_Rect rect {
            Sint16(off_x + m2p(impl->m_lastContact.x) - 3),
            Sint16(off_y + m2p(impl->m_lastContact.y) - 3),
            5, 5
        };
        SDL_RenderFillRect(dest, &rect);
    }
    if (impl->m_bypassModeActive) {
        SDL_Rect rect {
            Sint16(off_x + carData().x + (impl->m_bypassDirectionRight ? 0 : -15)),
            Sint16(off_y + carData().y),
            15, 2
        };
        SDL_RenderFillRect(dest, &rect);
    }
#endif
}
