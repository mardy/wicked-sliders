#include "car_model_factory.h"

#include "car_0.h"
#include "car_0_fixed.h"

#define IMG(ptr) reinterpret_cast<const EmbeddedImage*>(ptr)

const CarModel CarModelFactory::models[] = {
    {
        "Normal car",
        IMG(&img_car_0_fixed),
        IMG(&img_car_0),
        {
            0.4,        // max speed
            -0.02,      // min speed
            0.1,        // max acceleration
            1.0,        // linear damping
            1.0,        // angular damping
        }
    },
};

const int CarModelFactory::numModels =
    sizeof(CarModelFactory::models) / sizeof(CarModel);
